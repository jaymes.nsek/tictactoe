/*
 * Copyright (c) 2020. Written by James Nsek
 */

package com.ideatemax.tictactoe;

import android.app.Activity;
import android.os.CountDownTimer;
import android.widget.Button;
import android.widget.Toast;

import androidx.fragment.app.FragmentManager;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;

import static com.ideatemax.tictactoe.GameStats.FirstMover.RANDOM;
import static com.ideatemax.tictactoe.GameStats.FirstMover.X;
import static com.ideatemax.tictactoe.GameStats.PlayerMode.ONE;
import static com.ideatemax.tictactoe.GameStats.PlayerMode.TWO;
import static com.ideatemax.tictactoe.MainActivity.FLAG_WIN_FOUND;
import static com.ideatemax.tictactoe.MainActivity.GAME_DRAW;
import static com.ideatemax.tictactoe.MainActivity.mCod0;
import static com.ideatemax.tictactoe.MainActivity.mCod1;
import static com.ideatemax.tictactoe.MainActivity.mCod2;
import static com.ideatemax.tictactoe.Player.O_SYM;
import static com.ideatemax.tictactoe.Player.X_SYM;
import static com.ideatemax.tictactoe.Utils.BLOCK_FLAG;
import static com.ideatemax.tictactoe.Utils.WIN_FLAG;
import static com.ideatemax.tictactoe.Utils.winCases;


/**
 * Singleton logic class contains the ComputerMoves and GamePlay logic - singleton since it's data
 * is required to be synchronised among various App elements, and thus prevents instantiation of
 * multiple instances.
 */
final class LogicManager {
    /**
     * Debugging tag used by the Android logger.
     */
    private final String TAG = getClass().getSimpleName();

    private boolean mIsClickRegistered;

    // Holds the number of times button has been pressed to determine the stage in each round.
    private int mClickCount;

    private GameStats gameStats;

    private UiUpdateManager updateManager;

    private CountDownTimer mDelayRefresh;

    private MyActivityLifecycleCallbacks mCallbacks;

    private FragmentManager mFragmentManager;

    // Globally singleton instance of the class.
    public static LogicManager logicManager;


    /**
     * Private constructor to prevent instantiation through "new".
     */
    private LogicManager() {
        // Instantiate a instance objects upon which getters may be called later on for data.
        gameStats = GameStats.getInstance();

        updateManager = UiUpdateManager.getInstance();
    }

    /**
     * One-path instantiation process, returns the logicManager obj if non-null otherwise it is
     * created and returned.
     */
    public static LogicManager getInstance(){
        // Reactive initialisation.
        return logicManager == null ? logicManager = new LogicManager() : logicManager;
    }


    /**
     * Stores in this singleton class the callbacks and fragment manager's instances.
     * @param callbacks is the registered MainActivity's callbacks.
     * @param fragmentManager is MainActivity's "getSupportFragmentManager" used later as param
     *                        for DialogFragment.
     */
    public void storeCallbacksAndFragmentManagerInstances(MyActivityLifecycleCallbacks callbacks,
                                                          FragmentManager fragmentManager){
        mCallbacks = callbacks;
        mFragmentManager = fragmentManager;
    }


    public boolean getIsClickRegistered() {
        return mIsClickRegistered;
    }


    public void setIsClickRegistered(boolean isClickRegistered) {
        this.mIsClickRegistered = isClickRegistered;
    }


    public int getClickCount() {
        return mClickCount;
    }


    public void setClickCount(int clickCount) {
        this.mClickCount = clickCount;
    }


    /**
     * Increments current value of mClickCount by 1.
     */
    public void incClickCount() {
        this.mClickCount++;
    }




    /**
     * Inner LogicManager.GamePlay class holding all mediator methods necessary to induce various
     * game play states - such managing player turns.
     */
    public class GamePlay{
        /**
         * Debugging tag used by the Android logger.
         */
        private final String TAG = getClass().getSimpleName();


        /**
         * Public constructor.
         */
        public GamePlay() {

        }


        /**
         * Init FirstMove Player sym at game start or afterwards if user chooses to swap the
         * random assignment.
         */
        public void setFirstMoveAndGameParamsOnINIT(MyActivityLifecycleCallbacks callbacks) {
            // Retrieve variables for local use.
            Player playerO = gameStats.getPlayerO();
            Player playerX = gameStats.getPlayerX();

            // STEP 1. Designate starting player based on FirstMover val (RANDOM | X | O).
            GameStats.FirstMover firstMover = gameStats.getFirstMover();

            // Do the following if FirstMover is set to Random.
            if (firstMover == RANDOM) {
                // Generate a rand num between 0 and 1 inclusive.
                int anInt = new Random().nextInt(2);

                // PlayerX is designated FirstMover if rand == 0.
                gameStats.setCurrentPlayer((anInt == 0) ? X_SYM : O_SYM);
            }
            // If FirstMover is playerX, set currentPlayer as X.
            else if (firstMover == X) {
                gameStats.setCurrentPlayer(X_SYM);
            }
            // else FirstMover must be playerO, set currentPlayer as O.
            else {
                gameStats.setCurrentPlayer(O_SYM);
            }

            // Retrieve currentPlayer after the above update for local use.
            String curPlayer = gameStats.getCurrentPlayer();

            // Display current player.
            updateManager.displayCurrentPlayer();

            // STEP 2: Flag next round starter
            gameStats.setNextRoundFirstMover((curPlayer.equals(X_SYM)) ? O_SYM : X_SYM);

            // STEP 3. Manage settings for ONE or TWO playerMode.

            //Setup computer either as min or max player.
            if (gameStats.getPlayerMode() == ONE) {
                // Init freeGridsList, used by computer to identify playable grids.
                Utils.initialiseGridCoordinates(gameStats.getFreeGridsList());

                // Manage settings if playerX is the designated FirstMover
                if (curPlayer.equals(X_SYM)) {
                    // Set computer to minAlgo so plays as the minimising player and appropriately
                    // chooses the lowest value of probFactor.
                    playerO.setAlgorithm(Player.Algorithm.MIN);

                    // Set PlayerX Algorithm to opposite of playerO, invoking this method is also
                    // important for initialising player best move values.
                    playerX.setAlgorithm(Player.Algorithm.MAX);

                    // Enforce boolean is set to false so that User's click is registered.
                    setIsClickRegistered(false);
                }
                // else Computer is to make the FirstMove set it to Max-Algo
                // Note: O_SYM is always be Computer in onePlayer mode.
                else {
                    // Set getIsClickRegistered to TRUE so that User input is ignored.
                    setIsClickRegistered(true);

                    // Set computer to maxAlgo and playerX to minAlgo.
                    playerO.setAlgorithm(Player.Algorithm.MAX);

                    playerX.setAlgorithm(Player.Algorithm.MIN);

                    // Create a LaunchCountDownTimer for delaying refresh of screen.
                    // TODO: Consider incorporating the CDTimer into onComputerTurn.
                    LaunchCountDownTimer launchCDT =
                            new LaunchCountDownTimer(2500, 2000);

                    launchCDT.start();
                }
            }
            // Actions to take if game mode is equal to two players
            else {
                // Set Computer turn to false
                setIsClickRegistered(false);
            }
        }


        /**
         * After a valid grid input is registered in onGridClick() or onComputerTurn, checks for
         * a Win or Draw or a Continue or EndOfGame case.
         * @param gridKey the String representing the grid being played.
         */
        public void checkForWinOrDrawOrContinue(String gridKey) {
            // Increment the number of valid clicks.
            incClickCount();

            // init returnVal to false.
            boolean winMatchFound = false;

            // Get ref to objects for local use.
            HashMap<String, String> gameHashMap = (HashMap<String, String>) gameStats.getGameHashMap();

            Player pX = gameStats.getPlayerX();

            Player pO = gameStats.getPlayerO();


            // 1.- checkForWin()

            // use a for-each loop to itr over possible winCases combo.
            for (WinCase wincase : winCases) {
                // Extract the grid to a local instance and extract three coordinates for sanity check
                mCod0 = wincase.getFirst();
                mCod1 = wincase.getSecond();
                mCod2 = wincase.getThird();

                // Check for a winCombo match in textBayHMap:  if a1 = a2 = a3 for example.
                if (  (gameHashMap.containsKey(mCod0) && gameHashMap.containsKey(mCod1) &&
                        gameHashMap.containsKey(mCod2))
                        &&
                        (gameHashMap.get(mCod0).equals(gameHashMap.get(mCod1)) &&
                                gameHashMap.get(mCod1).equals(gameHashMap.get(mCod2)))  )
                {
                    // Indicate that a win match was found by setting boolean to TRUE.
                    winMatchFound = true;

                    // Highlight winCombo
                    updateManager.colour_or_decolour_winCombo(WinHighlighter.COLOUR);

                    // increment winner tally for current player.
                    if (gameStats.getCurrentPlayer().equals(X_SYM)) {
                        pX.incrementTally();

                        updateManager.updateTallyInfo(pX);
                    }
                    else {
                        pO.incrementTally();

                        updateManager.updateTallyInfo(pO);
                    }

                    //if a match is made, break away so that the other conditions aren't tested
                    break;
                }
            }

            // 2. - checkForDraw()

            // Init maximum allowed click/play count per round.
            int CLICK_MAX = 9;

            int currentRound = gameStats.getCurrentRound(),
                    numOfRounds = gameStats.getNumberOfRounds();

            // If there is a winMatch or mClickCount == 9, (i.e. signifying end of Round or Game)
            if (winMatchFound || getClickCount() == CLICK_MAX) {
                // 1. Handle end of game scenario.
                if (currentRound == numOfRounds) {
                    // Check winning player or draw, if current == end round then this is the end of
                    // the game, print the Winner or Draw scenario to Dialog offering
                    // PLAY AGAIN or EXIT GAME.
                    String winner, winnerName;
                    winnerName = "";

                    // A scenario where playerX wins.
                    if (pX.getTally() > pO.getTally()) {
                        // Set winner sym.
                        winner = pX.getSym();

                        // Prepare winnerName to send to Dialog.
                        winnerName = pX.getName();
                    }
                    // A scenario where playerO wins.
                    else if (pX.getTally() < pO.getTally()) {
                        // Set winner sym.
                        winner = pO.getSym();

                        // Prepare winnerName to send to Dialog.
                        winnerName = pO.getName();
                    }
                    // Game must be a draw.
                    else {
                        winner = GAME_DRAW;
                    }

                    // Invoke Dialog.
                    updateManager.showNoticeDialog(mFragmentManager, winner, winnerName,
                            numOfRounds, pX.getTally(), pO.getTally());
                }
                // 2. Handle end of Round: since mRound_Current != currentTotalNumOfRounds, check if
                // round was drawn or won and print out winner info accordingly.
                else {
                    // Init a CSequence for use in concatenating info that is to be printed.
                    CharSequence text;

                    // If no winning match made, round is a draw
                    if (!winMatchFound) {
                        text = "Round " + currentRound + " was drawn";
                    }
                    // else current player must have won
                    else {
                        // Prepare text to print depending on whether PlayerX or O is winner.
                        text = gameStats.getCurrentPlayer().equals(X_SYM) ?
                                (pX.getName() + " has won Round " + currentRound) :
                                (pO.getName() + " has won Round " + currentRound);
                    }

                    // Generate and then show Toast.
                    Toast.makeText(mCallbacks.getCurrentActivity(), text, Toast.LENGTH_LONG).show();

                    // Use CountDownTimer to delay refresh of screen so that User can review round
                    mDelayRefresh = new CountDownTimer(4000, 3000) {

                        public void onTick(long millisUntilFinished) {
                            // Do Nothing
                        }

                        public void onFinish() {
                            // CALL onNewRound() to apply the necessary methods for anew round.
                            onNewRound();
                        }
                    };
                    mDelayRefresh.start();
                }
            }

            // 3. - continueGame()

            // else no winMatch found and mClickCount != 9, then round isn't exhausted...
            else {
                // Remove played gridKey from playableGrids so as to to update the test cases
                // for next turn.
                if (gameStats.getPlayerMode() == ONE)
                    gameStats.getFreeGridsList().remove(gridKey);

                // call setNextPlayer to alternate player.
                setNextPlayer();
            }
        }


        /**
         * Method sets next player between turns.
         */
        public void setNextPlayer() {
            // Retrieve the current player.
            String justPlayed = gameStats.getCurrentPlayer();

            // If current player sym is X, set the next Current player as O - And Vice Versa
            gameStats.setCurrentPlayer(justPlayed.equals(X_SYM) ? O_SYM : X_SYM);

            // display mCurrentPlayer
            updateManager.displayCurrentPlayer();

            // Manage play setting depending on modes.
            if ( (gameStats.getPlayerMode() == TWO) ||
                    ((gameStats.getPlayerMode() == ONE) && justPlayed.equals(O_SYM)) )  {
                // register click as false so that buttons become pressable again.
                setIsClickRegistered(false);
            }
            // When in ONE mode and currentPlayer == O.
            else {
                // register click as true so that User clicks are ignored.
                setIsClickRegistered(true);

                // Create a LaunchCountDownTimer for delaying refresh of screen so that user has time
                // to reflect on info before next round begins and screen is refreshed.
                LaunchCountDownTimer launchCDT =
                        new LaunchCountDownTimer(2000, 1500);

                launchCDT.start();
            }
            // Enforce (Re)state of grids colour scheme in case a win Combo was highlighted to user.
            updateManager.colour_or_decolour_winCombo(WinHighlighter.DECOLOUR);
        }


        /**
         * On a new round, method calls various methods to enforce required params.
         */
        public void onNewRound() {
            // Empty HMap for new round.
            gameStats.clearGameHashMap();

            // Increment current Round by 1
            gameStats.setCurrentRound(gameStats.incCurrentRoundBy1());

            // Print updated round to UI.
            updateManager.updateRound();

            // Initialise click count to 0 at game commencement.
            setClickCount(0);

            // Clear grid views for new round.
            GridsContainer.getInstance().clearGridsViews();

            // Manage 1Player mode on new round initiation.
            if (gameStats.getPlayerMode() == ONE) {
                // (Re)init freeGridsList used by computer to identify playable grids.
                Utils.initialiseGridCoordinates(gameStats.getFreeGridsList());

                // Alternate Algorithm to reflect change in FirstMover and init BestMove values
                // to plus or minus infinity accordingly.
                gameStats.getPlayerO().alternateAlgorithm();
            }

            // Adopt and re-Flag next round starter
            if (gameStats.getNextRoundFirstMover().equals(X_SYM)) {
                // set currentPlayer = O, so that when setNextPlayer is called it'll
                // set the desired current player of X (a bit of overhead here!)
                gameStats.setCurrentPlayer(O_SYM);

                // Flag playerO as nextStarter
                gameStats.setNextRoundFirstMover(O_SYM);
            } else {
                gameStats.setCurrentPlayer(X_SYM);
                gameStats.setNextRoundFirstMover(X_SYM);
            }

            //Alternate between players
            setNextPlayer();
        }

    }




    /**
     * Inner LogicManager.ComputerMoves class acting as the brains of computer moves and as the
     * central manager it is responsible for populating appropriate Computer moves options into a
     * Map that is later handled by ComputerMoveSelector.java.
     */
    public class ComputerMoves {
        // TestBay Maps for testing the probability of an X or O win.
        private Map<String, String> xTestBayHashMap = new HashMap<>();

        private Map<String, String> xBlockTestBayHashMap = new HashMap<>();

        // TestBay Maps for testing & flagging block condition, where if O does not block X can win.
        private Map<String, String> oTestBayHashMap = new HashMap<>();


        /**
         * Package-private constructor.
         */
        ComputerMoves() { }


        /**
         *  Method is called from
         * setFirstMoveAndGameParamsOnINIT() on game start if Computer is registered as FirstMover
         * and subsequently from setNextPlayer(), a method for toggling between player turns.
         * @return is the grid coordinate
         */
        public String onComputerTurn() {
            // Retrieve ref to global variables for local use.
            ArrayList<String> freeGridsList = (ArrayList<String>) gameStats.getFreeGridsList();

            Player playerO = gameStats.getPlayerO();

            HashMap<String, String> gameHMap = (HashMap<String, String>) gameStats.getGameHashMap();

            // Sanity if-statements acts as additional precautionary measure that ensures that whilst
            // computer is computing here moves, user inputs aren't registered.
            if (getIsClickRegistered()) {

                // minMaxOption placed out of for-loop so that it is refreshed on each computation.
                Map<String, Integer> mMinMaxOptionsHMap = new HashMap<>();

                // Reset appropriateMove so that a new value can be flagged.
                playerO.resetBestMove();

                // Var for keeping tabs on probability factor.
                int probFactor;

                // Loop for generating bestAvailableMove based on DIFFICULTY setting, each
                // iteration test a the grid contained within the playable HMap.
                for (String testGridKey : freeGridsList) {
                    // Deep copy the gameHMap coordinates to xTest and oTest Bays for use in testing.
                    makeCopyFor(gameHMap, xTestBayHashMap);
                    makeCopyFor(gameHMap, oTestBayHashMap);
                    makeCopyFor(gameHMap, xBlockTestBayHashMap);

                    // insert K,V pair into TestBays
                    xTestBayHashMap.put(testGridKey, gameStats.getCurrentPlayer());
                    oTestBayHashMap.put(testGridKey, gameStats.getCurrentPlayer());

                    // Noting that playerO is also Computer, this HMap is used to test for grids
                    // playerX can legally check next to win if not blocked.
                    xBlockTestBayHashMap.put(testGridKey, X_SYM);

                    // Since the Computer (CurrentPlayer) is “O”, call testComputerWin() on
                    // oTestBayHashMap in its current form.
                    if (testComputerWin(oTestBayHashMap, false) == FLAG_WIN_FOUND)
                    {
                        // If TRUE, Assign the grid’s corresponding coordinate WIN_FLAG or -WIN_FLAG in
                        //  mMinMaxOptionsHMap and minMaxBestAlternative depending on whether Computer
                        //  is the playing role of min or maxAlgorithm.
                        probFactor = (playerO.getAlgorithm().equals(Player.Algorithm.MIN)) ?
                                (-1 * WIN_FLAG) :  WIN_FLAG;
                    }
                    // Checks to see if there are any grids that if not blocked player x will check.
                    else if (testComputerWin(xBlockTestBayHashMap, false)
                            == FLAG_WIN_FOUND)
                    {
                        // Assign BLOCK_FLAG or -BLOCK_FLAG (depending on whether Computer is playing
                        // role of min or maxAlgorithm) so it to flag it as highest priority
                        // after WIN_FLAG!
                        probFactor = (playerO.getAlgorithm().equals(Player.Algorithm.MIN)) ?
                                (-1 * BLOCK_FLAG) : BLOCK_FLAG;
                    }
                    // Else conduct a probability test on oTestBay versus xTestBay.
                    else {
                        // Make call to computeProb for deriving each individual player's probability,
                        // assigning result to xProb for playerX, and vice versa.
                        int xProb = computeProb(testGridKey, X_SYM,
                                freeGridsList, xTestBayHashMap);
                        int oProb = computeProb(testGridKey, playerO.getSym(),
                                freeGridsList, oTestBayHashMap);

                        // Compute computer's favourability and insert into OptionsHMap as K-V pairs,
                        // and using testGridKey as Key pair. To accurately calculate probFactor , the
                        // order must be maxPlayerPFactor - minPlayerPFactor. The choice of which
                        // ProbFactor formula to use is decided based on current player ALGORITHM.
                        probFactor = (playerO.getAlgorithm().equals(Player.Algorithm.MIN)) ?
                                (xProb - oProb) : (oProb - xProb);
                    }

                    // Update options accordingly.

                    // When playing as MIN_ALGO
                    if (playerO.getAlgorithm().equals(Player.Algorithm.MIN)) {
                        // Update the BestAlternative key, value pair
                        if (probFactor < playerO.getBestMoveVal()) {
                            playerO.setBestMove(testGridKey, probFactor);
                        }
                    }
                    // Player_O must be playing as MAX_ALGORITHM
                    else {
                        // Update the BestAlternative key, value pair
                        if (probFactor > playerO.getBestMoveVal()) {
                            playerO.setBestMove(testGridKey, probFactor);
                        }
                    }

                    // Add the win case to the list of option that the player can pick from if after
                    // iteration of the for-each loop no WIN or BLOCK is flagged..
                    if ((probFactor != WIN_FLAG) && (probFactor != (-1 * WIN_FLAG)) &&
                            (probFactor != BLOCK_FLAG) && (probFactor != (-1 * BLOCK_FLAG)))
                    {
                        mMinMaxOptionsHMap.put(testGridKey, probFactor);
                    }
                }

                // Call singleton class ComputerMoveSelector to select an appropriate moves.
                return ComputerMoveSelector.getInstance()
                        .pickFromSelectionBox(mMinMaxOptionsHMap, gameStats.getDifficulty());
            }

            return null;
        }


        /**
         * Computes the number of chances that player X or O has during this turn.
         * @param testGridKey is the currently tested string from playableGrids.
         * @param PlayerSymbol is the player for which the prob is conducted on.
         * @param freeGridsList current form of grid choices available for play.
         * @param testBay testing environment for simulating gameHashMap conditions.
         * @return resulting probFactor from sent params.
         */
        private int computeProb(String testGridKey, String PlayerSymbol,
                                List<String> freeGridsList, Map<String, String> testBay) {
            // Iterate through playable grids adding player symbols against them.
            for (String grid : freeGridsList) {
                // Only add grid if the grid is not the testGridKey
                if (!grid.equals(testGridKey)) {
                    testBay.put(grid, PlayerSymbol);
                }
            }
            // Then compute win for a probability factor and return this value back to calling method.
            return testComputerWin(testBay, true);
        }


        /**
         * Method makes a deep copy for a Map<String, String> of the current gameHashMap.
         * @param mapToCopyTo the HMap obj for which data is to be copied to.
         */
        private void makeCopyFor(Map<String, String> gameHashMap, Map<String, String> mapToCopyTo) {
            // call clear() on mapToCopyTo just in case it isn't empty, as would be in a CONTINUE case.
            mapToCopyTo.clear();

            // Check that gameHashMap is not empty (since trying to run an empty HMap thru a for-loop
            // will induce an Exception.
            if (!gameHashMap.isEmpty()) {
                // Begin the copying process
                for (Map.Entry<String, String> entry : gameHashMap.entrySet()) {
                    String key = entry.getKey();
                    String value = entry.getValue();
                    mapToCopyTo.put(key, value);
                }
            }
        }


        /**
         * Method tests simulated gameHashMap and is used by the AI to determine which gridKeys produce
         * a favourable case be that a WIN or BLOCK or PROB_FACTOR.
         * @param textBayHMap to simulate game conditions on.
         * @param checkAllMatchCases if TRUE then all WinMatches are analysed for otherwise after a
         *                           match is found the loop is broken out of.
         * @return an int value that either signifies NO_WIN, WIN_FOUND, or the num of matches flagged.
         */
        private int testComputerWin(Map<String, String> textBayHMap, boolean checkAllMatchCases) {
            // init the methods return val to zero.
            int numWinMatchFound = 0;

            // use a for-each loop to itr over possible winCases combo (8 Values).
            for (WinCase winCase : winCases) {
                // Assign each set of winCases coordinates to String var for use in the sanity check
                // that follows.
                String cod1 = winCase.getFirst();
                String cod2 = winCase.getSecond();
                String cod3 =winCase.getThird();

                // Check for a winCombo match in textBayHMap:  if a1 = a2 = a3 for example.
                if (  (textBayHMap.containsKey(cod1) && textBayHMap.containsKey(cod2) &&
                        textBayHMap.containsKey(cod3))
                        &&
                        (textBayHMap.get(cod1).equals(textBayHMap.get(cod2)) &&
                                textBayHMap.get(cod2).equals(textBayHMap.get(cod3)))  )
                {
                    //If checkAllMatchCases is FALSE, break away after a match is made so that other
                    // conditions aren't tested, the caveat is necessary the method can test for both
                    // "WinCaseFound" and "Probability of X or O" winning.
                    if (!checkAllMatchCases) {
                        // Indicate that a win match is found by setting returnVal to FLAG..
                        numWinMatchFound = FLAG_WIN_FOUND;
                        break;
                    }
                    // checkAllMatchCases must == TRUE, continue to check for the amount of winCases.
                    else {
                        // Indicate that a win match was found by increment the int by 1.
                        numWinMatchFound++;
                    }
                }
            }
            return numWinMatchFound;
        }


        /**
         * Method takes a grid coordinate returned from onComputerTurn and actions this by adding the
         * grid to gameHashMap and calling checkForWinOrDrawOrContinue() to test for the outcome.
         */
        public void playComputerMove(){
            GridsContainer container = GridsContainer.getInstance();

            Activity main = mCallbacks.getCurrentActivity();

            String gridString = onComputerTurn();

            GridsContainer.Grid gridEnum = GridsContainer.Grid.valueOf(gridString.toUpperCase());

            HashMap<String, String> gameHashMap =
                    (HashMap<String, String>) gameStats.getGameHashMap();

            // Register computer's move with symbol "O" in gameHashMap.
            gameHashMap.put(gridString, gameStats.getCurrentPlayer());

            // Take hold of View Object associated with the pressed gridString Button.
            Button button = (Button) container.getButtonFromId(gridEnum);

            // Display the char X or O.
            button.setText(gameHashMap.get(gridString));

            // If mIsSoundSwitchON_Current is set TRUE generate mpSound for computer (PlayerO).
            if (gameStats.getIsSoundSwitchON())
                SoundManager.getInstance(main, main.findViewById(R.id.soundToggleIBtn) )
                        .invokePlayerSound(main, gameStats.getCurrentPlayer());

            // Pass in latest pressed key to take appropriate action.
            new GamePlay().checkForWinOrDrawOrContinue(gridString);
        }
    }

}
