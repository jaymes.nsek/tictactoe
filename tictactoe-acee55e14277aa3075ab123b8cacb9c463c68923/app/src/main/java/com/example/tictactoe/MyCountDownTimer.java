package com.example.tictactoe;

import android.os.CountDownTimer;

public class MyCountDownTimer extends CountDownTimer {

    /** FIELDS/INSTANCE VARIABLES **/
    // Not required as we must call super and store the values through it, we can however create
    // additional fields to those offered by the superclass here!!

    /** CONSTRUCTOR **/
    public MyCountDownTimer (long millisInFuture, long countDownInterval){
        super(millisInFuture, countDownInterval);
    }

    @Override
    public void onTick(long millisUntilFinished) {

    }

    @Override
    public void onFinish() {

    }
}
