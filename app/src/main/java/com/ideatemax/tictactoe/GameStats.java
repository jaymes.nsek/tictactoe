/*
 * Copyright (c) 2020. Written by James Nsek
 */

package com.ideatemax.tictactoe;

import android.util.Log;

import androidx.annotation.NonNull;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 *Singleton class holds game statistics information, vital for game play.
 */
final class GameStats {
    /**
     * Debugging tag used by the Android logger.
     */
    private final String TAG = getClass().getSimpleName();

    private PlayerMode mPlayerMode;

    private Player playerX = new Player("X");

    private Player playerO = new Player("O");

    private String mCurrentPlayer;

    private String mNextRoundFirstMover;

    private Difficulty mDifficulty;

    private Integer mCurrentRound = 0;

    private Integer mNumberOfRounds = 0;

    private FirstMover mFirstMover;

    private Boolean mIsSoundSwitchON = false;

    // HashMap for storing k,v pairs of gridCoordinates alongside the symbols associated with them.
    private  Map<String, String> gameHashMap;

    // Free grids (aka Playable grids), contain the list of coordinates yet to have been played.
    private List<String> mFreeGridsList = new ArrayList<>();

    // Single allowable instance of this class.
    private static GameStats gameStats;

    /**
     * Private constructor to disallow instantiation outside this class with "new".
     */
    private GameStats() { gameHashMap = new HashMap<>(); }


    /**
     * Lazy initialisation - params are not instantiated!.
     */
    public static GameStats getInstance(){
        return (gameStats == null) ? gameStats = new GameStats() : gameStats;
    }


    /**
     * Helper method with the option to init all params in one call after obj creation -
     * non primitive type data used since throwNPE(Object object) is later made to return Obj.
     */
    public void setGameStats(PlayerMode playerMode, FirstMover firstMover, Difficulty difficulty,
                             Integer numOfRounds, Boolean isSoundSwitchON){
        // Instantiate object if null.
        if (gameStats == null) gameStats = new GameStats();

        // Init all game stat variables.
        mPlayerMode = playerMode;
        mFirstMover = firstMover;
        mDifficulty = difficulty;
        mNumberOfRounds = numOfRounds;
        mCurrentRound = 1;
        mIsSoundSwitchON = isSoundSwitchON;
    }


    public PlayerMode getPlayerMode() {
        return (mPlayerMode == null) ? (PlayerMode) throwNPE(null) : mPlayerMode;
    }


    public void setPlayerMode(PlayerMode playerMode) {
        mPlayerMode = playerMode;
    }


    public Difficulty getDifficulty() {
        return (mDifficulty == null) ? (Difficulty) throwNPE(null) : mDifficulty;
    }

    public void setDifficulty(Difficulty difficulty) {
        mDifficulty = difficulty;
    }


    /**
     * @return is primitive int data type rather than Integer to allow direct use in code!
     */
    public int getCurrentRound() {
        return mCurrentRound == 0 ? (int) throwNPE(mCurrentRound): mCurrentRound;
    }

    public void setCurrentRound(int currentRound) {
        mCurrentRound = currentRound;
    }


    /**
     * Method increments the currentRound value by 1 and then returns the update value if the
     * condition matches, else an IllegalStateException is thrown.
     */
    public int incCurrentRoundBy1() {
        String msg = " In order to use incCurrentRoundBy1() currentRound must be less than " +
                "totalNumberOfRounds";

        if (mCurrentRound < mNumberOfRounds) return ++mCurrentRound;
        else throw new IllegalStateException(msg);
    }


    public int getNumberOfRounds() {
        return mNumberOfRounds == 0 ? (int) throwNPE(mNumberOfRounds) : mNumberOfRounds;
    }


    public FirstMover getFirstMover() {
        return mFirstMover == null ? (FirstMover) throwNPE(null) : mFirstMover;
    }


    public boolean getIsSoundSwitchON() {
        return mIsSoundSwitchON;
    }


    public void setIsSoundSwitchON(boolean isSoundSwitchON) {
        mIsSoundSwitchON = isSoundSwitchON;
    }


    public String getCurrentPlayer() {
        return mCurrentPlayer;
    }


    public void setCurrentPlayer(String currentPlayer) {
        mCurrentPlayer = currentPlayer;
    }


    public String getNextRoundFirstMover() {
        return mNextRoundFirstMover;
    }


    public void setNextRoundFirstMover(String nextRoundFirstMover) {
        mNextRoundFirstMover = nextRoundFirstMover;
    }


    public Map<String, String> getGameHashMap() {
        return gameHashMap;
    }


    public void setGameHashMap(Map<String, String> gameHashMap) {
        this.gameHashMap = gameHashMap;
    }


    public void clearGameHashMap() {
        this.gameHashMap.clear();
    }


    public List<String> getFreeGridsList() {
        return mFreeGridsList;
    }


    public void setFreeGridsList(List<String> freeGridsList) {
        mFreeGridsList = freeGridsList;
    }


    public Player getPlayerX() {
        return playerX;
    }


    public Player getPlayerO() {
        return playerO;
    }

    
    /**
     * Helper NullPointerException method.
     * @param object is the name of the object the caller is trying to throw NPE on.
     */
    private Object throwNPE(Object object) throws NullPointerException{
        try{
            // Throw NPE Exception.
            throw new NullPointerException("GameStats Obj uninitialised, it must be non-NULL.");
        } catch (NullPointerException e) {
            // Log NPE.
            Log.e(TAG, "throwNPE: ", e);
        }
        return object;
    }


    /**
     * Enum inner class for setting playerMode to ONE player or TWO players.
     */
    public enum PlayerMode {
        // Calls constructor with
        ONE ("1 Player") {
            @Override
            @NonNull
            public String toString() {
                return "ONE";
            }
        },

        TWO ("2 Players"){
            @Override
            @NonNull
            public String toString() {
                return "TWO";
            }
        };

        private final String uiString;

        /**
         * Constructor for initialising UI String.
         * @param uiString is the UI String to initialise against the CONST.
         */
        PlayerMode(String uiString){
            this.uiString = uiString;
        }

        /**
         * Since toString and constant name are used interchangeably in project a different
         * string that is to be show to UI is added.
         * @return is the UI String which is displayed.
         */
        public String toUiString() {
            return uiString;
        }
    }


    /**
     * Enum inner class for setting first mover arguments within GameStats.
     */
    public enum FirstMover {
        RANDOM {
            @Override
            @NonNull
            public String toString() {
                return "RANDOM";
            }
        },

        X {
            @Override
            @NonNull
            public String toString() {
                return "X";
            }
        },

        O {
            @Override
            @NonNull
            public String toString() {
                return "O";
            }
        }
    }


    /**
     * Enum inner class for setting difficulty arguments within GameStats.
     */
    public enum Difficulty {
        LOW {
            @Override
            @NonNull
            public String toString() {
                return "LOW";
            }
        },

        MID {
            @Override
            @NonNull
            public String toString() {
                return "MID";
            }
        },

        HIGH {
            @Override
            @NonNull
            public String toString() {
                return "HIGH";
            }
        }
    }

}