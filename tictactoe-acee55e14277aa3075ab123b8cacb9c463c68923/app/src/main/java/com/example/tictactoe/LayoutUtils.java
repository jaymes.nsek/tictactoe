package com.example.tictactoe;


import android.app.Activity;
import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

public class LayoutUtils {

    /** FIELDS **/
    //Logging tag
    private static final  String TAG = "LayoutUtils";

    public static Grids[] winCases = new Grids[8];
    // the height and weight dimension for achieving a square grid whilst utilizing screen space
    public static int gridSize;


    /** CONSTRUCTOR**/
    private LayoutUtils() {
        throw new AssertionError();
    }

    //TODO - THis method is not working, but there is an added problem... dimensions are according
    // to Android docs in pixels not dp, therefore it will also need to be converted, perhaps try
    // retrieving phone pixel density information... and use this var along with other params to
    // determine dp value.

    /** When method is called, get the width dp and set the height equal to it **/
    public static void setWidthDynamically(View view, int gridSize){
        Button thisView = (Button) view;
        //Init mParams to 0,0 since the constructor needs values
        ViewGroup.LayoutParams mParams = thisView.getLayoutParams();
        //set the width and height using the passed gridSize value
        mParams.width = gridSize;
        mParams.height = gridSize;
        //set the dimensions
        thisView.setLayoutParams(mParams);
    }

    /** Compute Grid width/height based on device screen info **/
    public static int computeGridSize(Activity mActivity){
        //TODO 2 - Is it possible to retrieve available screen resolution/dp during on create, and
        // use this information to set the grids?

        //TODO - Call this method in either onStart()/onResume rather than onCreate so that each
        // time focus resumes screen layout is reconfigured
        int gutter = 8 * 3; //value in dp, 16dp x 3 gutters
        int margin = 16 * 2; //value in dp, 16dp x 2 margins
        int columnCount = 3; //nondimentional

        int widthPixels = mActivity.getResources().getDisplayMetrics().widthPixels;
        int density_dpi = mActivity.getResources().getDisplayMetrics().densityDpi;
        float scaled_density = mActivity.getResources().getDisplayMetrics().scaledDensity;
        int guttersAndMarginsInDip = gutter + margin;
        float guttersAndMarginsInPixel = (float) guttersAndMarginsInDip * scaled_density;
        //Round up value to an integer
        int guttersAndMarginsInPixel_int = (int) guttersAndMarginsInPixel;
        // Add an extra 8dp for cushioning effect since screen size is not exact
        float cushion = 8 * scaled_density; // value in dp
        int cushionInt = (int) cushion;
        // return the calculatedGridSize, IOW the height and weight dimension for achieving
        // a square grid whilst utilizing screen space
        return (widthPixels - (guttersAndMarginsInPixel_int)) / 3;
    }

    /** Initialises win cases **/
    public static void initialiseWinCases(){
        winCases[0] = new Grids("a1", "a2", "a3");
        winCases[1] = new Grids("a1", "b1", "c1");
        winCases[2] = new Grids("a1", "b2", "c3");
        winCases[3] = new Grids("a2", "b2", "c2");
        winCases[4] = new Grids("a3", "b3", "c3");
        winCases[5] = new Grids("b1", "b2", "b3");
        winCases[6] = new Grids("a3", "b2", "c1");
        winCases[7] = new Grids("c1", "c2", "c3");
    }

    /** Compute Layout **/
    public static void computeLayout(Activity activityToStart){
        //TODO - Move this to onStart() so that each time focus resumes, views placement is recalculated
        int gutter = 8 * 3; //value in dp, 16dp x 3 gutters
        int margin = 16 * 2; //value in dp, 16dp x 2 margins
        int columnCount = 3; //nondimentional

        int widthPixels = activityToStart.getResources().getDisplayMetrics().widthPixels;
        int density_dpi = activityToStart.getResources().getDisplayMetrics().densityDpi;
        float scaled_density = activityToStart.getResources().getDisplayMetrics().scaledDensity;
        int guttersAndMarginsInDip = gutter + margin;
        float guttersAndMarginsInPixel = (float) guttersAndMarginsInDip * scaled_density;
        //Round up value to an integer
        int guttersAndMarginsInPixel_int = (int) guttersAndMarginsInPixel;
        // Add an extra 8dp for cushioning effect since screen size is not exact
        float cushion = 8 * scaled_density; // value in dp
        int cushionInt = (int) cushion;
        gridSize = (widthPixels - (guttersAndMarginsInPixel_int)) / 3;
    }


}
