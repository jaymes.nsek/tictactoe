/*
 * Copyright (c) 2020. Written by James Nsek
 */

package com.ideatemax.tictactoe;

import android.app.Activity;
import android.util.Log;
import android.widget.TextView;

public class OtherViewsContainer {
    /**
     * Debugging tag used by the Android logger.
     */
    private final String TAG = getClass().getSimpleName();

    public static final String TV_KEY_CURRENT_PLAYER = "current_player_view";
    public static final String TV_KEY_ROUND_VAL = "round_value";
    public static final String TV_KEY_X_NAME = "player_x_tally_name";
    public static final String TV_KEY_O_NAME = "player_o_tally_name";
    public static final String TV_KEY_X_TALLY = "player_x_tally_score";
    public static final String TV_KEY_O_TALLY = "player_o_tally_score";
    public static final String TV_KEY_MODE_DIFF = "mode_and_diff_display";

    private TextView[] otherViews;

    private static OtherViewsContainer other;


    /**
     * Private constructor.
     */
    private OtherViewsContainer() {}


    public static OtherViewsContainer getInstance(){
        return (other == null) ? other = new OtherViewsContainer() : other;
    }


    public void cacheTextViews(MyActivityLifecycleCallbacks callbacks) {
        Activity act = callbacks.getCurrentActivity();

        try {
            if (act != null) {
                otherViews = new TextView[]{
                        act.findViewById(R.id.current_player_view),
                        act.findViewById(R.id.round_value),
                        act.findViewById(R.id.player_x_tally_name),
                        act.findViewById(R.id.player_o_tally_name),
                        act.findViewById(R.id.player_x_tally),
                        act.findViewById(R.id.player_o_tally),
                        act.findViewById(R.id.mode_and_diff_display)
                };
            }
            else {
                throw new NullPointerException();
            }
        } catch (NullPointerException e) {
            Log.e(TAG, "cacheTextViews: ", e);
        }
    }


    /**
     * Return view object matching the passed gridKey
     **/
    public TextView getTextView(String name) {
        // Set to one for error detection in code, if the program continually prints to a1 then
        // switch statement below has errors.
        try{
            if (otherViews == null){
                throw new Exception();
            }

            switch(name){
                case TV_KEY_CURRENT_PLAYER:
                    return otherViews[0];
                case TV_KEY_ROUND_VAL:
                    return otherViews[1];
                case TV_KEY_X_NAME:
                    return otherViews[2];
                case TV_KEY_O_NAME:
                    return otherViews[3];
                case TV_KEY_X_TALLY:
                    return otherViews[4];
                case TV_KEY_O_TALLY:
                    return otherViews[5];
                case TV_KEY_MODE_DIFF:
                    return otherViews[6];
                default:
                    throw new Exception();
            }
        } catch (Exception e){
            Log.e(TAG, "getTextView: ", e);
        }
        return null;
    }
}
