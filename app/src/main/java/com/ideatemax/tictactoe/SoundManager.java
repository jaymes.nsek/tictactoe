/*
 * Copyright (c) 2020. Written by James Nsek
 */

package com.ideatemax.tictactoe;

import android.content.Context;
import android.media.MediaPlayer;
import android.util.Log;
import android.view.View;
import android.widget.ImageButton;

import androidx.annotation.NonNull;
import androidx.core.content.ContextCompat;

import static com.ideatemax.tictactoe.Player.X_SYM;


/**
 * Class for managing all matters relating to App sound - Final for preventing the use of 'extends'.
 */
final class SoundManager {

    /**
     * Debugging tag used by the Android logger.
     */
    private final String TAG = getClass().getSimpleName();

    private final static String S_TAG = "SoundManager";
    
    // MediaPlayer object for making a switch sound as we toggle.
    private MediaPlayer mediaPlayer;

    private static GameStats gameStats;

    // Globally singleton instance of the class.
    public static SoundManager sound;

    /**
     * Private constructor to prevent instantiation with "new" keyword outside of class.
     */
    private SoundManager() { }


    /**
     * One-path instantiation process, returns the sound obj if non-null otherwise it is
     * created and returned.
     */
    public static SoundManager getInstance(@NonNull Context context,
                                           @NonNull View soundToggleBtn){
        // Reactive initialisation.
        return  (sound == null) ? setOnSoundToggleClickListener(context, soundToggleBtn) : sound;
    }
    

    /**
     * SoundManager object for use in invoking indicating sound toggle registration when user toggles
     * between ON and OFF.
     */
    public void invokeToggleSound(Context context) {
        // propel mediaPlayer to a Prepared state
        mediaPlayer = MediaPlayer.create(context, R.raw.drill_switch);

        // set an onPreparedListener and when invoked start playing sound.
        mediaPlayer.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {
            @Override
            public void onPrepared(MediaPlayer mp) {
                // play sound.
                mp.start();
            }
        });

        // set an onCompletionListener so that resources may be released once finished with.
        mediaPlayer.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
            @Override
            public void onCompletion(MediaPlayer mp) {
                // release resources.
                mp.release();
            }
        });
    }

    
    /**
     * Plays SoundManager registered to players on grid click
     */
    public void invokePlayerSound(Context context, String currentPlayer) {
        // Instigate either Prepared state depending on whether playerX or O is currentPlayer.
        if (currentPlayer.equals(X_SYM)) {
            mediaPlayer = MediaPlayer
                    .create(context, R.raw.mario_jumping_mike_koenig);
        } else {
            mediaPlayer = MediaPlayer
                    .create(context, R.raw.strong_punch_mike_koenig);
        }

        // set an onPreparedListener and when invoked start playing sound.
        mediaPlayer.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {
            @Override
            public void onPrepared(MediaPlayer mp) {
                // play sound.
                mp.start();
            }
        });

        // set an onCompletionListener so that resources may be released once finished with.
        mediaPlayer.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
            @Override
            public void onCompletion(MediaPlayer mp) {
                // release resources.
                mp.release();
            }
        });
    }

    
    /**
     * Play Dialog sound on end of game if a WIN is registered.
     */
    public void invokeOnGameWinSound(Context context) {
        // propel mediaPlayer to a Prepared state
        mediaPlayer = MediaPlayer.create(context, R.raw.short_triumphal_fanfare_john_stracke);

        // set an onPreparedListener and when invoked start playing sound.
        mediaPlayer.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {
            @Override
            public void onPrepared(MediaPlayer mp) {
                // play sound.
                mp.start();
            }
        });

        // set an onCompletionListener so that resources may be released once finished with.
        mediaPlayer.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
            @Override
            public void onCompletion(MediaPlayer mp) {
                // release resources.
                mp.release();
            }
        });
    }

    
    /**
     * Play Dialog sound on end of game if a DRAW is registered.
     */
    public void invokeOnDrawnSound(Context context) {
        // propel mediaPlayer to a Prepared state
        mediaPlayer = MediaPlayer.create(context, R.raw.basketball_buzzer_sound_bible_dot_com);

        // set an onPreparedListener and when invoked start playing sound.
        mediaPlayer.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {
            @Override
            public void onPrepared(MediaPlayer mp) {
                // play sound.
                mp.start();
            }
        });

        // set an onCompletionListener so that resources may be released once finished with.
        mediaPlayer.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
            @Override
            public void onCompletion(MediaPlayer mp) {
                // release resources.
                mp.release();
            }
        });
    }


    /**
     * Method is called in "getInstance" - when sound == null, hence "sound" is initialised
     *  and return after setting the listener.
     * @param view is the SoundImageButton housed in the activity_main.xml.
     * @return is an instantiated SoundManager Object.
     */
    private static SoundManager setOnSoundToggleClickListener(final Context context,
                                                              final View view){
        ImageButton soundBtn = (ImageButton) view;

        // Enforce that no previous listeners exists.
        soundBtn.setOnClickListener(null);

        soundBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // Action to carry out when listener is called-back.

                onSoundToggleClicked(context, view);
                Log.i(S_TAG, "onClick: onSoundToggleClicked invoked");
            }
        });

        return sound = new SoundManager();
    }


    /**
     * Method is linked to MainActivity's sound toggle ImageButton and toggles sound ON and OFF.
     * @param view is the SoundImageButton housed in the activity_main.xml.
     */
    private static void onSoundToggleClicked(Context context, View view) {
        // Get the current switch state.
        boolean isSoundOn = GameStats.getInstance().getIsSoundSwitchON();

        ImageButton soundIBtn = (ImageButton) view;
        
        // Conduct MediaPlayer create procedures and set Listeners that manage sound execution.
        SoundManager.getInstance(context, soundIBtn).invokeToggleSound(context);
        
        // When sound is currently ON and button press is registered, do the following:
        if (isSoundOn) {
            // Alternate current state to false.
            GameStats.getInstance().setIsSoundSwitchON(false);

            // Display muted image.
            soundIBtn.setImageResource(android.R.drawable.ic_lock_silent_mode);

            // Set drawable tint
            soundIBtn.setColorFilter(ContextCompat.getColor(context, R.color.no_sound_tint));
        }
        else {
            // Alternate current state to true.
            GameStats.getInstance().setIsSoundSwitchON(true);

            // Display sound on image.
            soundIBtn.setImageResource(android.R.drawable.ic_lock_silent_mode_off);

            // Set drawable tint
            soundIBtn.setColorFilter(ContextCompat.getColor(context, R.color.sound_tint));
        }

        // Enforce soundIBtn background and background colour in any event.
        soundIBtn.setBackground(null);

        soundIBtn.setBackgroundColor(
                ContextCompat.getColor(context, R.color.main_backGrd_colour));
    }

}
