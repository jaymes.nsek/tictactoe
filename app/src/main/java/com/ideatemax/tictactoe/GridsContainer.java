/*
 * Copyright (c) 2020. Written by James Nsek
 */

package com.ideatemax.tictactoe;

import android.app.Activity;
import android.util.Log;
import android.widget.Button;

final class GridsContainer {
    /**
     * Debugging tag used by the Android logger.
     */
    private final String TAG = getClass().getSimpleName();

    private static GridsContainer container;

    private Button[] gridsBtns;


    /**
     * Private constructor to prevent instantiation outside this singleton class.
     */
    private GridsContainer() {}

    public static GridsContainer getInstance(){
        return (container == null) ? container = new GridsContainer() : container;
    }


    /**
     * Since gridsBtn cannot be referenced from a static point, method must be call separately
     * after instance creation to initialise the caching process.
     */
    public  void cacheGrids(MyActivityLifecycleCallbacks callbacks) {
        Activity activity = callbacks.getCurrentActivity();
        if (activity != null){
            gridsBtns = new Button[]{ activity.findViewById(R.id.a1),
                    activity.findViewById(R.id.a2), activity.findViewById(R.id.a3),
                    activity.findViewById(R.id.b1), activity.findViewById(R.id.b2),
                    activity.findViewById(R.id.b3), activity.findViewById(R.id.c1),
                    activity.findViewById(R.id.c2), activity.findViewById(R.id.c3) };
        }
        else{
            Log.i(TAG, "displayTextFromNonActivityClass: Activity is null!");
        }
    }


    /**
     * Return view object matching the passed gridKey
     **/
    public Button getButtonFromId(Grid grid) {
        // Set to one for error detection in code, if the program continually prints to a1 then
        // switch statement below has errors.

        String gridString = grid.toString();

        int buttonIndex = 0;
        switch (gridString) {
            case "a1":
                buttonIndex = 0;
                break;
            case "a2":
                buttonIndex = 1;
                break;
            case "a3":
                buttonIndex = 2;
                break;
            case "b1":
                buttonIndex = 3;
                break;
            case "b2":
                buttonIndex = 4;
                break;
            case "b3":
                buttonIndex = 5;
                break;
            case "c1":
                buttonIndex = 6;
                break;
            case "c2":
                buttonIndex = 7;
                break;
            case "c3":
                buttonIndex = 8;
        }
        return gridsBtns[buttonIndex];
    }


    /**
     * Method enforces that all grids are cleared before a new round/game begins.
     */
    public void clearGridsViews() {
        for (Button button : gridsBtns) {
            button.setText(R.string.null_entry);
        }
    }
        

    /**
     * Enum constant for use with getButtonFromId to proof params.
     */
    public enum Grid {
        A1,
        A2,
        A3,
        B1,
        B2,
        B3,
        C1, 
        C2, 
        C3;

        @Override
        public String toString() {
            return super.toString().toLowerCase();
        }
    }
}
