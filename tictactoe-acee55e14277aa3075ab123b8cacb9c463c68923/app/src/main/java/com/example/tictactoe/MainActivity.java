package com.ideatemax.tictactoe;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.fragment.app.DialogFragment;

import java.util.HashMap;
import java.util.Map;
import java.util.Random;

import static com.ideatemax.tictactoe.LayoutUtils.gridSize;
import static com.ideatemax.tictactoe.LayoutUtils.winCases;

public class MainActivity extends LifecycleLoggingActivity
        implements GameWonDialogFragment.NoticeDialogListener {

    /**
     * Debugging tag used by the Android logger.
     */
    private final String TAG = getClass().getSimpleName();

    /**
     *  =========================== FIELDS / INSTANCE VARIABLES ===================================
     */
    private boolean clickRegistered;
    private String currentPlayer;
    private final static String NO_ID = "no_id";
    //empty HashMap for storing k,v pairs
    private Map <String, String> gridHMap = new HashMap<>();
    //declare Button array for latter cache
    private Button[] myButtons = new Button[11];

    private TextView mDisplayCurrentPlayerTextView;
    private TextView mUpdateRoundTextView;
    private TextView mTallyPlayerX_TextView;
    private TextView mTallyPlayerO_TextView;
    private TextView mModeAndDiffTextView;

    public final static String GAME_DRAW = "draw";
    public final static String ROUND_DRAWN = "draw";
    // Holds the number of times button has been pressed, if after the 8th press win condition is
    // not met, then game must be a draw.
    private int clickCount;
    private final int CLICK_MAX = 8;
    private int xTally;
    private int oTally;
    public static final String PLAYER_X = "X";
    public static final String PLAYER_O = "O";
    public static final String RANDOM_START = "randstart" ;
    private CountDownTimer delayRefresh;
    // Intent fields
    private static final int SETTINGS_REQUEST = 123;
    public static final String EXTRA_PLAYER_MODE = "player";
    public static final String EXTRA_DIFFICULTY = "difficulty";
    public static final String EXTRA_STARTING_PLAYER = "starter";
    public static final String EXTRA_ROUND_NUM = "rounds";
    public static final String EXTRA_SOUND_SWITCH = "soundswitch";
    public static final String EXTRA_SOUND = "sound";
    // Global values
    public static final String ONE_PLAYER = "singleplayer";
    public static final String TWO_PLAYERS = "twoplayers";
    public static final String LOW_DIFFICULTY = "low difficulty";
    public static final String MID_DIFFICULTY = "mid difficulty";
    public static final String HIGH_DIFFICULTY = "high difficulty";
    public static final int ONE_ROUND = 1;
    public static final int THREE_ROUND = 3;
    public static final int SEVEN_ROUND = 7;
    public static final int ELEVEN_ROUND = 11;
    public static final boolean SOUND_ON = true;
    public static final boolean SOUND_OFF = false;
    public static final String BUZZ_SOUND = "buzz";
    public static final String BELL_SOUND = "bell";
    public static final String XMAS_SOUND = "xmas";

    // Game setting fields - initialised in onCreate()
    private String currentPlayerMode;
    private String currentDifficulty;
    private int currentRound;
    private int currentNumOfRounds;
    private String currentStartingPlayer;
    private boolean currentSoundSwitch;
    private String currentSound;



    /** =================================== ACTIVITY LIFE CYCLE ===================================
     * @param savedInstanceState object that contains saved state information.
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        // Call gameStart() to initialise default or user specified setting if overridden
        gameStart();
    }

    @Override
    protected void onStart() {
        super.onStart();
        adoptNewSettings();
    }



    /**
     * =================================== MAIN =================================================
     * @param view
     */
    public void onClick(View view){
        if (!clickRegistered){
            //set clickRegistered to true so that the program is not bombarded by multiple clicks
            clickRegistered = true;
            //var is used more than once in code, store it locally to avoid multiple calls
            String buttonName = retrieveViewName(view);
            // if statement checks button ID is NOT already associated with a press OR has NO_ID
            if ( !buttonName.equals(NO_ID)  && !gridHMap.containsKey(buttonName) ){
                // Increment the number of valid clicks
                clickCount ++;
                //store current registered player symbol "X" or "O" for the button in HashMap
                gridHMap.put(buttonName, currentPlayer);
                //take hold of View Object
                Button mPressedGrid = (Button) view;
                //Display the char X or O
                mPressedGrid.setText(gridHMap.get(buttonName));
                //Call checkForWinOrDrawOrContinue() to take appropriate action
               checkForWinOrDrawOrContinue();
            }
            else {
                // register click as false so that buttons become pressable again
                clickRegistered = false;
            }
        }
    }



    /**
     *  ============================ LIFECYCLE HELPER METHODS =====================================
     */

    //Call on onCreate() or onResume() hook method, when the Activity is about to become visible
    // to set the layout for MainActivity
    public void setViewsHeight(){
        for (int i = 1; i < 10; i++ ){
            // Itr thru the Array of Buttons and set the height = width
            LayoutUtils.setWidthDynamically(myButtons[i], gridSize);
        }
    }

    private void assignDefaultFieldsOnCreate() {
        // set the current and end round numbers to 1 | The end round num is changable by user via
        // settings.
        currentRound = 1;
        currentPlayerMode = TWO_PLAYERS;
        currentDifficulty = MID_DIFFICULTY;
        currentStartingPlayer = RANDOM_START;
        currentNumOfRounds = THREE_ROUND;
        currentSoundSwitch = SOUND_OFF;
        currentSound = BUZZ_SOUND;
    }

    // TODO - Since gameStart(), adoptNewSetting() and ganeContinue() have many similar methods and
    //  fields, try and employ polymorphism gameContinue( ... ) | gameStart( gameContinue; ... )
    public void gameStart(){
        //If current HMap is NOT empty, which it should be as onCreate() is called only once at
        // Activity creation.
        if (!gridHMap.isEmpty()){
            gridHMap.clear();
        }
        // initialise vital game parameters
        cacheViews();
        //initialise the win cases
        LayoutUtils.initialiseWinCases();
        // compute layout
        LayoutUtils.computeLayout(this);
        setViewsHeight();
        //TODO - having rand & x & o case is currently redundant of user cannot save setting in
        // persistent storage!! Implement this is future?
        //Init default settings, this MUST be placed before the setPlayerGameStart() here otherwise
        // currentStart would not have been init and this may through errors!
        assignDefaultFieldsOnCreate();
        // Set current player at game start.
        setPlayerGameStart(currentStartingPlayer);
        // print out the round info
        updateRound();
        // TODO - Set the game up so that if two player mode is selected only mode shows and when
        //  one player is selected, both the mode and diff show.
        //print current mode and difficulty setting
        updateModeAndDiff();
        // initialise click count to 0 at game commencement
        clickCount = 0;
        // init winning tally and call update tally to display on screen.
        refreshTally();
        // click registered to false so that game becomes playable, do it last.
        clickRegistered = false;
    }



    /**
     * ================================ MainActivity HELPER METHODS ===============================
     */

    // Cache the views on onCreate so that they are locally ready to be set.
    public void cacheViews(){
        // cache grid buttons so that they are close by.
        myButtons[1] = findViewById(R.id.a1);
        myButtons[2] = findViewById(R.id.a2);
        myButtons[3] = findViewById(R.id.a3);
        myButtons[4] = findViewById(R.id.b1);
        myButtons[5] = findViewById(R.id.b2);
        myButtons[6] = findViewById(R.id.b3);
        myButtons[7] = findViewById(R.id.c1);
        myButtons[8] = findViewById(R.id.c2);
        myButtons[9] = findViewById(R.id.c3);
        // Cache current_player_tag && round_display_tag && player_tallies.
        mDisplayCurrentPlayerTextView = findViewById(R.id.current_player_tag);
        mUpdateRoundTextView = findViewById(R.id.round_display);
        mTallyPlayerX_TextView = findViewById(R.id.player_x_tally);
        mTallyPlayerO_TextView = findViewById(R.id.player_o_tally);
        mModeAndDiffTextView = findViewById(R.id.mode_and_diff_display);
    }

    //Cache the views on onCreate so that they are locally ready to be set
    public void clearViews(){
        for (int i = 1; i < 10; i++) {
            myButtons[i].setText(R.string.null_entry);
        }
    }

    public void gameContinue(){
        // Empty HMap for new round.
        if (!gridHMap.isEmpty()){
            gridHMap.clear();
        }
        // increment current Round by 1 and update this
        currentRound++;
        updateRound();
        // initialise click count to 0 at game commencement
        clickCount = 0;
        // clear views
        clearViews();
        //Alternate between players
        setNextPlayer();
        // set click registered last to false so that grids become clickable again
        clickRegistered = false;
    }

    public void adoptNewSettings() {
        // Refresh HMap for new game
        if (!gridHMap.isEmpty()){
            gridHMap.clear();
        }
        // Set current player at game start.
        setPlayerGameStart(currentStartingPlayer);
        // Update modeAndDiff info
        updateModeAndDiff();
        // Update round.
        updateRound();
        // initialise click count to 0 at game commencement
        clickCount = 0;
        // clear views
        clearViews();
        // init winning tally and call update tally to display on screen.
        refreshTally();
        // enforce click registered to false so that game becomes playable, do it last.
        clickRegistered = false;
    }

    private void updateRound(){
        String text = "Round: " + currentRound + " of " + currentNumOfRounds;
        mUpdateRoundTextView.setText(text);
    }

    private void updateModeAndDiff(){
        // Update Mode and Difficulty TextView
        String text;
        if ( currentPlayerMode.equals(ONE_PLAYER) ){
            text = currentPlayerMode + "  |  " + currentDifficulty;
        }
        else {
            text = currentPlayerMode;
        }
        mModeAndDiffTextView.setText(text);
    }

    private void refreshTally () {
        // init winning tally and call update tally to display on screen.
        xTally = 0;
        oTally = 0;
        updateTally("X");
        updateTally("O");
    }



    /**
     * =========================== Game Logic HELPER METHODS ======================================
     */

    public String retrieveViewName(View view){
        if (view.getId() == View.NO_ID) return NO_ID;
        else return view.getResources().getResourceEntryName(view.getId());
    }

    public void setNextPlayer(){
        //TODO - Turns should be alternated automatically when playing different rounds in same
        // game so when in round mode, keep tally of whose turn it is to start next...
        if (currentPlayer.equals("X")){
            // Toggle between X and 0 for current player depending on previous player
            currentPlayer = "O";
        }
        else {
            currentPlayer = "X";
        }
        displayCurrentPlayer();
        // register click as false so that buttons become pressable again
        clickRegistered = false;
    }

    public void displayCurrentPlayer(){
        String text = "Player: " + currentPlayer;
        mDisplayCurrentPlayerTextView.setText(text);
    }

    // init Player 1 sym at game start or afterwards if user chooses to swap the random assignment.
    public void setPlayerGameStart(String startProc){
        //TODO - Implement a random() func here so game start either X or O starts
        //TODO - Also link a function where the user can decide which player starts, this will
        // require this method to take a parameter

        // Declare and instantiate a new Random object
        Random rand = new Random();
        int whoGoesNext = rand.nextInt(2);

        if ( startProc.equals(RANDOM_START) ) {
            if (whoGoesNext == 0){
                currentPlayer = PLAYER_X;
            }
            else {
                currentPlayer = PLAYER_O;
            }
        }
        else if ( startProc.equals(PLAYER_X) ) {
            currentPlayer = PLAYER_X;
        }
        else {
            currentPlayer = PLAYER_O;
        }
        // Display current player
        displayCurrentPlayer();
    }

    //Update round scores to screen when a win case is matched
    private void updateTally(String player){

        if ( player.equals("X") ) {
            String text = "Player X: " + xTally;
            mTallyPlayerX_TextView.setText(text);
        }
        else {
            String text = "Player O: " + oTally;
            mTallyPlayerO_TextView.setText(text);
        }
    }

    //After a valid grid input is registered in onClick(), checks for a Win or Draw and a Continue
    // or EndOfGame cases.
    public void checkForWinOrDrawOrContinue() {
        boolean winMatchFound = false;
        for (int i = 0; i < winCases.length; i++){
            // Extract the grid to a local instance and extract three coordinates for sanity check
            Grids myLocal = winCases[i];
            String cod1 = myLocal.getFirst();
            String cod2 = myLocal.getSecond();
            String cod3 = myLocal.getThird();

            /** CHECKS FOR A GAME_WIN CONDITION **/
            //Checks if A1 = A2 = A3 - .get() comes the values held, we are testing for "X" and "O"
            if (  ( gridHMap.containsKey(cod1) && gridHMap.containsKey(cod2 ) &&
                    gridHMap.containsKey(cod3) ) && ( gridHMap.get(cod1).equals(gridHMap.get(cod2))
                    && gridHMap.get(cod2 ).equals(gridHMap.get(cod3)) )  ) {
                // Indicate that a win match was found by setting boolean to TRUE.
                winMatchFound = true;
                // increment winner tally for current player.
                if (currentPlayer.equals(PLAYER_X)) {
                    xTally++;
                    updateTally("X");
                }
                else {
                    oTally++;
                    updateTally("O");
                }
                //if a match is made, break away so that the other conditions aren't tested
                break;
            }
        }

        // If there is a winMatch or clickCount == 8, (i.e. signifying end of Round or Game)
        if (winMatchFound || clickCount == CLICK_MAX) {
            if (currentRound == currentNumOfRounds){
                /** THIS HANDLES END OF GAME **/
                // Check winning player or draw, if current == end round then this is the end of
                // the game, print the Winner or Draw scenario to Dialog offering
                // PLAY AGAIN or EXIT GAME.
                String winner;
                if (xTally > oTally){
                    winner = "X";
                }
                else if (oTally > xTally){
                    winner = "O";
                }
                else {
                    winner = GAME_DRAW;
                }
                showNoticeDialog(winner);
            }
            else {
                CharSequence text;
                Context context = getApplicationContext();
                int duration = Toast.LENGTH_LONG;

                // If 8 clicks have been registered and no winning match made, round is a draw
                if (clickCount == CLICK_MAX && winMatchFound) {
                    text = "Round " + currentRound + " is drawed";
                }
                // else current player must have won
                else {
                    text = "Player " + currentPlayer + " has won Round "
                            + currentRound;
                }
                // Generate and then show Toast.
                Toast toast = Toast.makeText(context, text, duration);
                toast.show();

                // Use CountDownTimer to delay refresh of screen so that User can review round
                // TODO - Try setting CD interval to 10ms so that we effectively block UI thread...
                //  will this solve the problem of lagging?
                delayRefresh = new CountDownTimer(3000, 1000) {

                    public void onTick(long millisUntilFinished) {
                       // Try Printing a Toast countdown msg, e.g. Round 2 starts in 5, 4, 2, 1, 0...
                    }

                    public void onFinish() {
                        /** CALL gameContinue() to do the following **/
                        // Reset clickCount to zero for a fresh round.
                        // Clear grid HashMap
                        //increment current (next) round by 1.
                        // clear grids for a fresh game.
                        // continue game by alternating nextPlayer
                        gameContinue();
                    }
                };
                delayRefresh.start();
            }
        }
        // If no winMatch found and clickCount != 8, then round isn't exhausted... setNextPLayer()
        else {
            setNextPlayer();
        }
    }


    /**
     * ===================== METHODS FOR HANDLING WIN DIALOG UI ===================================
     */
    public void showNoticeDialog(String winner) {
        // Create an instance of the dialog fragment and show it
        DialogFragment dialog = new GameWonDialogFragment(winner);
        dialog.show(getSupportFragmentManager(), "GameWonDialogFragment");
    }

    // The dialog fragment receives a reference to this Activity through the
    // Fragment.onAttach() callback, which it uses to call the following methods
    // defined by the NoticeDialogFragment.NoticeDialogListener interface
    @Override
    public void onDialogPositiveClick(DialogFragment dialog) {
        // User touched the dialog's positive button
        // Call gameStart() to re-initialise default or user specified setting if overridden
        gameStart();
        // Clear the button views from the previous game
        clearViews();
        // Refresh scores for new Game
        refreshTally();
    }

    @Override
    public void onDialogNegativeClick(DialogFragment dialog) {
        // User touched the dialog's negative button
        this.finish();
    }



    /**
     * =============================== INTENT TO START SETTINGSACTIVITY ===========================
     **/

    // Callable when user presses on settings button
    public void onClickSettings(View view) {
        Intent intent = new Intent(this, SettingsFullScreenActivity.class);
        intent.putExtra(EXTRA_PLAYER_MODE, currentPlayerMode)
                .putExtra(EXTRA_DIFFICULTY, currentDifficulty)
                .putExtra(EXTRA_STARTING_PLAYER, currentStartingPlayer)
                .putExtra(EXTRA_ROUND_NUM, currentNumOfRounds)
                .putExtra(EXTRA_SOUND_SWITCH, currentSoundSwitch)
                .putExtra(EXTRA_SOUND, currentSound);
        startActivityForResult(intent, SETTINGS_REQUEST);
    }

    /**
     * Called back when finish() is called on SettingsActivity.
     * @param requestCode
     * @param resultCode
     * @param data
     */
    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        // Check if the started Activity was successfully completed.
        if (resultCode == Activity.RESULT_OK) {
            // Check if the requestCode matches what was sent out and retrieve data.
            retrieveReturnedSettings(requestCode, data);
        }
        // Check if the started Activity did not successfully complete
        // and print error indication to TextView on screen
        else{
            // Else resultCode must be CANCELLED
            retrieveReturnedSettings(requestCode, data);
            // Print toast to indicate that user cancelled setting process,
            // whilst protecting against NullPointerException.
            if (data.getStringExtra("reason") != null){
                CharSequence text = data.getStringExtra("reason");
                Context context = getApplicationContext();
                int duration = Toast.LENGTH_LONG;
                // Generate and then show Toast.
                Toast toast = Toast.makeText(context, text, duration);
                toast.show();
            }
        }
    }

    public void retrieveReturnedSettings(int requestCode, Intent data){
        if (requestCode == SETTINGS_REQUEST) {
            // Guard against NullPointerException
            if ( data.getStringExtra(EXTRA_PLAYER_MODE) != null &&
                    data.getStringExtra(EXTRA_DIFFICULTY) != null &&
                    data.getStringExtra(EXTRA_STARTING_PLAYER) != null &&
                    data.getStringExtra(EXTRA_SOUND) != null )
            {
                // Assign the new settings to the game.
                currentRound = 1;
                currentPlayerMode = data.getStringExtra(EXTRA_PLAYER_MODE);
                currentDifficulty = data.getStringExtra(EXTRA_DIFFICULTY);
                currentNumOfRounds = data.getIntExtra(EXTRA_ROUND_NUM, 1);
                currentStartingPlayer = data.getStringExtra(EXTRA_STARTING_PLAYER);
                currentSoundSwitch = data.getBooleanExtra(EXTRA_SOUND_SWITCH, false);
                currentSound = data.getStringExtra(EXTRA_SOUND);
            }
        }
    }
}
