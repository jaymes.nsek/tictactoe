/*
 * Copyright (c) 2020. Written by James Nsek
 */

package com.ideatemax.tictactoe;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.fragment.app.DialogFragment;

import java.util.HashMap;

import static com.ideatemax.tictactoe.GameStats.Difficulty.MID;
import static com.ideatemax.tictactoe.GameStats.FirstMover.RANDOM;
import static com.ideatemax.tictactoe.GameStats.PlayerMode.*;
import static com.ideatemax.tictactoe.Player.PHONE_O_NAME;

/**
 * App entry point and it is thus the mediator between the various elements of code.
 */
public class MainActivity extends LifecycleLoggingActivity
        implements GameWonDialogFragment.NoticeDialogListener {
    /**
     * Debugging tag used by the Android logger.
     */
    private final String TAG = getClass().getSimpleName();

    private final static String NO_ID = "no_id";

    public final static String GAME_DRAW = "draw";

    public static final String EXTRA_PLAYER_MODE = "player";
    
    public static final String EXTRA_DIFFICULTY = "difficulty";
    
    public static final String EXTRA_FIRST_MOVER = "starter";
    
    public static final String EXTRA_TOTAL_ROUNDS = "rounds";

    public static final String EXTRA_X_NAME = "playernamex";
    
    public static final String EXTRA_O_NAME = "playernameo";

    public static final int ONE_ROUND = 1;
    
    public static final int THREE_ROUND = 3;
    
    public static final int SEVEN_ROUND = 7;
    
    public static final int ELEVEN_ROUND = 11;
    
    public static final boolean SOUND_OFF = false;

    private GameStats gameStats;

    public final static int FLAG_WIN_FOUND = 8008;

    // Declare SoundClass Object for managing all MediaPlayer objects
    private SoundManager mSoundManager;

    // Flag SettingsIntent invocation
    boolean settingsIntentInvoked;

    // checkForWinOrDrawOrContinue win combo highlighting global variables
    public static String mCod0, mCod1, mCod2;

    // Instances of the Manager classes.
    private LogicManager mLogicManager;

    private LogicManager.GamePlay gamePlay;

    private UiUpdateManager updateManager;

    private final MyActivityLifecycleCallbacks mCallbacks = new MyActivityLifecycleCallbacks();

    private GridsContainer gridsContainer;

    private OtherViewsContainer viewsContainer;

    /**
     * @param savedInstanceState object that contains saved state information.
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        // Register callback before calling super.
        getApplication().registerActivityLifecycleCallbacks(mCallbacks);

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        // Init winCombo highlighting global variables to a1, a2, a3 - Required here otherwise
        // markOrUnmark..() indirectly call in onGameStart() will cause App crash.
        mCod0 = "a1";
        mCod1 = "a2";
        mCod2 = "a3";

        mLogicManager = LogicManager.getInstance();

        // Register MainActivity callbacks.
        mLogicManager.storeCallbacksAndFragmentManagerInstances(mCallbacks, getSupportFragmentManager());

        // Call to SoundManager to init a global obj and set an OnClickListener on SoundToggle btn.
        mSoundManager
                = SoundManager.getInstance(this, findViewById(R.id.soundToggleIBtn));

        // Game stat must be init before other onGameStart params so that her values are non-NULL.
        gameStats = GameStats.getInstance();

        gamePlay = mLogicManager.new GamePlay();

        updateManager = UiUpdateManager.getInstance();

        updateManager.registerForCallbacksStates(mCallbacks);

        // Call onGameStart to initialise default or user specified setting if overridden
        try {
            // Send callback to container so that Views are cached for global access.
            gridsContainer = GridsContainer.getInstance();
            gridsContainer.cacheGrids(mCallbacks);

            viewsContainer = OtherViewsContainer.getInstance();
            viewsContainer.cacheTextViews(mCallbacks);

            onGameStart();
        } catch (Exception e) {
            Log.e(TAG, "onCreate: ", e);
        }

        // init flag for settingsIntentInvoked to false
        settingsIntentInvoked = false;

        // Set clicked listener.
        Utils.setOnShareClickedListener(this, (ImageButton) findViewById(R.id.share_ibtn));

        Utils.setOnSettingsClickedListener(MainActivity.this,
                (ImageButton) findViewById(R.id.settings_ibtn));
    }


    @Override
    protected void onStart() {
        super.onStart();
        // Flag on reStart() hook call so that onNewSettings is applied only via settingsIntent.
        if (settingsIntentInvoked) {
            onNewSettings();

            // Re-flag intentInvoked to false so that on Activity losing focus NOT via intent,
            // activity is not refreshed.
            settingsIntentInvoked = false;
        }
    }


    /**
     * This method manages grid presses by user and helps ensure that presses are synchronised.
     * @param view Button view pressed by user.
     */
    public void onGridClick(View view) {
        // Get local ref to gamesHashMap.
        HashMap<String, String> gameHashMap = (HashMap<String, String>) gameStats.getGameHashMap();

        // Check that click has not already been registered and being processed.
        if (!mLogicManager.getIsClickRegistered()) {
            //set mIsClickRegistered to true so that the program is not bombarded by multiple clicks
            mLogicManager.setIsClickRegistered(true);

            //Retrieve button's coordinate/designation from View object.
            String buttonName = retrieveViewName(view);

            // Check button ID is NOT already associated with a press or has NO_ID
            if (!buttonName.equals(NO_ID) && !gameHashMap.containsKey(buttonName)) {
                //store current registered player symbol "X" or "O" for the button in HashMap
                gameHashMap.put(buttonName, gameStats.getCurrentPlayer());

                //take hold of View Object
                Button mPressedGrid = (Button) view;

                //Display the char X or O
                mPressedGrid.setText(gameHashMap.get(buttonName));

                // when in TWO_PLAYER mode - Generate mpSound for playerX and playerO ().
                // First check whether mIsSoundSwitchON_Current is set to TRUE.
                if (gameStats.getIsSoundSwitchON()) {
                    mSoundManager.invokePlayerSound(this, gameStats.getCurrentPlayer());
                }

                //Call checkForWinOrDrawOrContinue() passing in latest set grid and
                // take appropriate action.
                gamePlay.checkForWinOrDrawOrContinue(buttonName);
            } else {
                // register click as false so that buttons becomes pressable again.
                mLogicManager.setIsClickRegistered(false);
            }
        }
    }


    /**
     * Method is called in onCreate() on game start to initialise vital params.
     */
    private void onGameStart() {
        // init default values
        initDefaultSettings();

        //initialise the win cases
        Utils.initialiseWinCases();

        // Call onNewSetting to apply vital game commencement values.
        onNewSettings();
    }


    /**
     * Init's default settings in onCreate() Hook method.
     */
    private void initDefaultSettings() {
        // Set default params on game launch.
        gameStats.setGameStats(ONE, RANDOM, MID, THREE_ROUND, SOUND_OFF);

        // Update PlayerO name to "Phone" When in OnePlayerMode.
        gameStats.getPlayerO().setName(PHONE_O_NAME);
    }


    /**
     * This method is mainly targeted at SettingsActivity, after new settings have been applied and
     * return intent received, the method is called in onStart() to apply new settings to Main.
     */
    public void onNewSettings() {
        // (re)initialise mRound_Current to 1.
        gameStats.setCurrentRound(1);

        // Enforce Empty HMap on new game.
        gameStats.clearGameHashMap();

        // Set current player at game start plus other vital params.
        gamePlay.setFirstMoveAndGameParamsOnINIT(mCallbacks);

        UiUpdateManager manager = UiUpdateManager.getInstance();

        // Update modeAndDiff info.
        manager.updateModeAndDiff();

        // Update round.
        manager.updateRound();

        // initialise click count to 0 at game commencement.
        mLogicManager.setClickCount(0);

        // clear views.
        gridsContainer.clearGridsViews();

        // init winning tally and call update tally to display on screen.
        resetTally();

        // Un-mark mCod grids (winning grids) after a gameWonDialog has been triggered.
        manager.colour_or_decolour_winCombo(WinHighlighter.DECOLOUR);
    }


    /**
     * Method (re)sets player X and O's scores (tally) to zero on new settings received.
     */
    public void resetTally() {
        // Enforce player obj reset of tally to zero.
        gameStats.getPlayerX().setTally(0);
        gameStats.getPlayerO().setTally(0);

        //viewsContainer = OtherViewsContainer.getInstance();

        // Update new tally to screen.
        updateManager.updateTallyInfo(gameStats.getPlayerX());

        updateManager.updateTallyInfo(gameStats.getPlayerO());
    }


    /**
     * Method is useful for managing cached views locally.
     * @param view grid Button obj whose view is being requested.
     * @return the designated id "aka name" of view as a String.
     */
    public String retrieveViewName(View view) {
        return (view.getId() == View.NO_ID) ? NO_ID :
                view.getResources().getResourceEntryName(view.getId());
    }


    /**
     * DialogFragment Overrides: The dialog fragment receives a reference to this Activity through
     * the Fragment.onAttach() callback, which it uses to call the following methods defined by the
     * NoticeDialogFragment.NoticeDialogListener interface.
     */

    @Override
    public void onDialogPositiveClick(DialogFragment dialog) {
        // User touched the dialog's positive button
        // Call onNewSettings to re-initialise default or user specified setting if overridden.
        onNewSettings();
    }

    @Override
    public void onDialogNegativeClick(DialogFragment dialog) {
        // User touched the dialog's negative button, shutDown App.
        this.finish();
    }


    /**
     * Called back when finish() is called on SettingsActivity and a return intent is sent to
     * MainActivity.
     * @param requestCode SETTINGS_REQUEST with which the original Intent was sent.
     * @param resultCode OK or Cancel
     * @param data the transmitted result.
     */
    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        // flag settingIntentInvoked to true so that activity is refreshable with new
        // data in onStart().
        settingsIntentInvoked = true;

        // Check if the started Activity was successfully completed.
        if (resultCode == Activity.RESULT_OK) {
            // Check if the requestCode matches what was sent out and retrieve data.
            retrieveReturnedSettings(requestCode, data);
        }
        // Check if the started Activity did not successfully complete
        // and print error indication to TextView on screen
        else {
            // Else resultCode must be CANCELLED
            retrieveReturnedSettings(requestCode, data);

            // Print toast to indicate that user cancelled setting process,
            // whilst protecting against NullPointerException.
            if (data.getStringExtra("reason") != null) {

                CharSequence text = data.getStringExtra("reason");

                // Generate and then show Toast.
                Toast.makeText(this, text, Toast.LENGTH_LONG).show();
            }
        }
    }


    /**
     * Helper method for handling requestCode and if a match, retrieves Intent data.
     * @param requestCode SETTINGS_REQUEST with which the original Intent was sent.
     * @param data the transmitted result from SettingsActivity.
     */
    public void retrieveReturnedSettings(int requestCode, Intent data) {
        // Checks whether request code matches
        int SETTINGS_REQUEST = 123;

        if (requestCode == SETTINGS_REQUEST) {
            // Guard against NullPointerException
            if (data.getStringExtra(EXTRA_PLAYER_MODE) != null &&
                    data.getStringExtra(EXTRA_DIFFICULTY) != null &&
                    data.getStringExtra(EXTRA_FIRST_MOVER) != null)
            {
                // Assign the new settings to the game.
                gameStats.setGameStats(
                        GameStats.PlayerMode.valueOf(data.getStringExtra(EXTRA_PLAYER_MODE)),
                        GameStats.FirstMover.valueOf(data.getStringExtra(EXTRA_FIRST_MOVER)),
                        GameStats.Difficulty.valueOf(data.getStringExtra(EXTRA_DIFFICULTY)),
                        data.getIntExtra(EXTRA_TOTAL_ROUNDS, 1),
                        gameStats.getIsSoundSwitchON() );

                gameStats.getPlayerX().setName(data.getStringExtra(EXTRA_X_NAME));

                gameStats.getPlayerO().setName(data.getStringExtra(EXTRA_O_NAME));
            }
        }
    }


    @Override
    protected void onDestroy() {
        super.onDestroy();

        // Unregister after calling super to avoid memory leaks.
        getApplication().unregisterActivityLifecycleCallbacks(mCallbacks);

        // Nullify objects.
        viewsContainer = null;
        gridsContainer = null;
        mLogicManager = null;
    }
}
