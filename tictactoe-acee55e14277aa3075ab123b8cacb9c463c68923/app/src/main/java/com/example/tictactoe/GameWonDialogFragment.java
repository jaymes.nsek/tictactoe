package com.example.tictactoe;

import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;

import androidx.appcompat.app.AlertDialog;
import androidx.fragment.app.DialogFragment;

import static com.example.tictactoe.MainActivity.GAME_DRAW;

public class GameWonDialogFragment extends DialogFragment {

    /**
     * INSTANCE VARIABLES / FIELDS
     */
    private String winner;

    /**
     * PUBLIC CONSTRUCTOR
     */
    public GameWonDialogFragment(String winner){
        this.winner = winner;
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        // Build the dialog and set up the button click handlers
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());

        // Using the value of "winner" determine whether a draw msg is printed or otherwise
        String msg;
        if (winner.equals(GAME_DRAW)){
            msg = getString(R.string.game_drawn);
        }
        else {
            msg = getString(R.string.win_msg_left) + " " + winner + " " +
                    getString(R.string.win_msg_right);
        }

        builder.setMessage(msg)
                .setPositiveButton(R.string.continue_playing, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        // Send the positive button event back to the host activity
                        listener.onDialogPositiveClick(GameWonDialogFragment.this);
                    }
                })
                .setNegativeButton(R.string.quit_game, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        // Send the negative button event back to the host activity
                        listener.onDialogNegativeClick(GameWonDialogFragment.this);
                    }
                });
        return builder.create();
    }

    /* INNER INTERFACE CLASS - GameWonDialogFragment.NoticeDialogListener
     * The activity that creates an instance of this dialog fragment must
     * implement this interface in order to receive event callbacks.
     * Each method passes the DialogFragment in case the host needs to query it. */
    public interface NoticeDialogListener {
        public void onDialogPositiveClick(DialogFragment dialog);
        public void onDialogNegativeClick(DialogFragment dialog);
    }

    // Use this instance of the interface to deliver action events - composed in onCreateDialog()
    private NoticeDialogListener listener;

    // Override the Fragment.onAttach() method to instantiate the NoticeDialogListener
    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        // Verify that the host activity implements the callback interface
        try {
            // Instantiate the NoticeDialogListener so we can send events to the host
            listener = (NoticeDialogListener) context;
        } catch (ClassCastException e) {
            // The activity doesn't implement the interface, throw
            // TODO Check back to see that "this." works instead of using Activity.
            throw new ClassCastException(this.toString()
                    + " must implement NoticeDialogListener");
        }
    }
}
