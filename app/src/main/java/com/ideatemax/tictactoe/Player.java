/*
 * Copyright (c) 2020. Written by James Nsek
 */

package com.ideatemax.tictactoe;


import androidx.annotation.NonNull;

public class Player {
    // FIELDS/INSTANCE VARIABLES
    private String sym;
    private int tally;
    private String name;
    private Algorithm algorithm;

    public static final String X_SYM = "X";
    public static final String O_SYM = "O";
    public static final String PLAYER_X_NAME = "Player X";
    public static final String PLAYER_O_NAME = "Player O";
    public static final String PHONE_O_NAME = "Phone";

    // Assign +/- 1M to simulate minus and plus infinity
    public static final int MINUS_INFINITY = -1000000;
    public static final int PLUS_INFINITY = +1000000;

    // BestMove data.
    private String key;
    private int value;


    // PUBLIC CONSTRUCTOR
    public Player(String sym){
        // On construction set playing sym to either X or O
        this.sym = sym;
        this.name = "Player " + sym;
        this.tally = 0;
    }

    /**
     * Overridden toString method for use with debugging.
     * @return concatenated class info.
     */
    @NonNull
    @Override
    public String toString() {
        return "Player " + getName() + " " + "BestMove " + this.key + " - " + this.value;
    }


    // SETTERS

    /**
     * Set min or max Algorithm
     * @param algorithm min or max ENUM.
     */
    public void setAlgorithm(Algorithm algorithm) {
        // Set Player Algorithm.
        this.algorithm = algorithm;

        // Initialise bestMove accordingly.
        int algo = algorithm.equals(Algorithm.MIN) ? PLUS_INFINITY : MINUS_INFINITY;

        setBestMove("e5", algo);
    }


    /**
     * setName mutable only via settingsActivity
     * @param name name chosen by player.
     */
    public void setName(String name) {
        this.name = name;
    }


    /**
     * sets tally value.
     * @param tally current player score.
     */
    public void setTally(int tally) {
        this.tally = tally;
    }


    /**
     * increments tall by 1.
     */
    public void incrementTally() {
        tally++;
    }


    /**
     * Method for setting best move key and value simultaneously.
     * @param key grid coordinate key.
     * @param value the probability factor value.
     */
    public void setBestMove(String key, int value) {
        this.key = key;
        this.value = value;
    }


    /**
     * Reset bestMove in-between turns to round assignment
     */
    public void resetBestMove() {
        // Used to (re)init bestMove flagging mechanism. Since bestMove is reset only when we call
        // alternateAlgorithm() in onNewRound(), therefore this is used for resetting between turns

        int algo = algorithm.equals(Algorithm.MAX) ? MINUS_INFINITY : PLUS_INFINITY;

        setBestMove("e5", algo);
    }


    /**
     * Method is used for alternating max-min algorithm between rounds.
     */
    public void alternateAlgorithm(){
        if (algorithm.equals(Algorithm.MIN)){
            this.algorithm = Algorithm.MAX;
           setBestMove("e5", MINUS_INFINITY);
        }
        else {
            this.algorithm = Algorithm.MIN;
           setBestMove("e5", PLUS_INFINITY);
        }
    }


    // GETTERS

    public int getTally() {
        return tally;
    }

    public String getSym() {
        return sym;
    }

    public Algorithm getAlgorithm() {
        return algorithm;
    }

    public String getBestMoveKey() {
        return this.key;
    }

    public int getBestMoveVal() {
        return this.value;
    }

    public String getName() {
        return name;
    }


    // Nested ENUM
    // Used for the sole purpose of restricting choices to the values therein.
    public enum Algorithm{
        MAX, MIN
    }

}
