package com.example.tictactoe;

import java.util.HashMap;

import static com.example.tictactoe.MainActivity.LOW_DIFFICULTY;
import static com.example.tictactoe.MainActivity.MID_DIFFICULTY;

public class Player {
    /** FIELDS/INSTANCE VIARIABLES **/
    private String symbol;
    private String algorithm;
    private static final String MIN_ALGORITHM = "minAlgorithm";
    private static final String MAX_ALGORITHM = "maxAlgorithm";
    private String gameDifficulty;
    private int minmaxBestAlternative;
    // Assign +/- 1M to simulate minus and plus infinity
    private static final int MINUS_INFINITY = -1000000;
    private static final int PLUS_INFINITY = +1000000;


    /** PUBLIC CONSTRUCTOR **/
    public Player(String symbol){
        // On construction set playing symbol to either X or O
        this.symbol = symbol;
    }


    /** SET METHODS **/
    // Set min or maxx Algorithm
    public void setAlgorithm(String algorithm) {
        this.algorithm = algorithm;
        if (algorithm.equals(MIN_ALGORITHM)){
            minmaxBestAlternative = PLUS_INFINITY;
        }
        else {
            minmaxBestAlternative = MINUS_INFINITY;
        }
    }

    // Set player symbol (aka character "X" or "O")
    public void setSymbol(String symbol) {
        this.symbol = symbol;
    }

    // TODO - Implement difficulty Algorithm in connection with selectGrid()
    // Set difficulty Algorithm
    public void setDifficulty(String gameDifficulty){
        this.gameDifficulty = gameDifficulty;
    }

    /** HELPER METHODS **/
    public void selectGrid(HashMap<String, String> minmaxOptionsHMap){
        // Local class - Managing difficuly levels
        class Local {
            public void applyDifficulty(){
                if (gameDifficulty.equals(MID_DIFFICULTY)){
                    // MID DIFF ALGORITHM
                }
                else if (gameDifficulty.equals(LOW_DIFFICULTY)){
                    // LOW DIFF ALGORITHM
                }
                else {
                    // HIGH DIFF ALGORITHM
                }
            }
        }

        // Procedure: If minAlgorithm is activated
        if( algorithm.equals(MIN_ALGORITHM) ) {

        }

        // Procedure: If maxAlgorithm is activated (IOW set to true)
        else {

        }
    }


}
