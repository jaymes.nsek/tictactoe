package com.example.tictactoe;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Switch;

import static com.example.tictactoe.MainActivity.BELL_SOUND;
import static com.example.tictactoe.MainActivity.BUZZ_SOUND;
import static com.example.tictactoe.MainActivity.ELEVEN_ROUND;
import static com.example.tictactoe.MainActivity.EXTRA_DIFFICULTY;
import static com.example.tictactoe.MainActivity.EXTRA_PLAYER_MODE;
import static com.example.tictactoe.MainActivity.EXTRA_ROUND_NUM;
import static com.example.tictactoe.MainActivity.EXTRA_SOUND;
import static com.example.tictactoe.MainActivity.EXTRA_SOUND_SWITCH;
import static com.example.tictactoe.MainActivity.EXTRA_STARTING_PLAYER;
import static com.example.tictactoe.MainActivity.HIGH_DIFFICULTY;
import static com.example.tictactoe.MainActivity.LOW_DIFFICULTY;
import static com.example.tictactoe.MainActivity.MID_DIFFICULTY;
import static com.example.tictactoe.MainActivity.ONE_PLAYER;
import static com.example.tictactoe.MainActivity.ONE_ROUND;
import static com.example.tictactoe.MainActivity.SEVEN_ROUND;
import static com.example.tictactoe.MainActivity.SOUND_OFF;
import static com.example.tictactoe.MainActivity.SOUND_ON;
import static com.example.tictactoe.MainActivity.THREE_ROUND;
import static com.example.tictactoe.MainActivity.TWO_PLAYERS;
import static com.example.tictactoe.MainActivity.PLAYER_X;
import static com.example.tictactoe.MainActivity.PLAYER_O;
import static com.example.tictactoe.MainActivity.RANDOM_START;
import static com.example.tictactoe.MainActivity.XMAS_SOUND;


public class SettingsFullScreenActivity extends LifecycleLoggingActivity {

    /** FIELDS **/
    // Old settings
    private String previousPlayerMode;
    private String previousDifficulty;
    private int previousNumOfRounds;
    private String previousStartingPlayer;
    private boolean previousSoundSwitch;
    private String previousSound;
    // New settings
    private String newPlayerMode;
    private String newDifficulty;
    private int newNumOfRounds;
    private String newStartingPlayer;
    private boolean newSoundSwitch;
    private String newSound;

    private boolean defaultToggled;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_settings_full_screen);

        // Retrieve incoming intent extras
        decipherIncomingIntent();

        // Assign previous setting to current, so that if user cancels the process we may simple
        // reinstate it
        setNewPlayerMode(previousPlayerMode);
        newDifficulty = previousDifficulty;
        newNumOfRounds = previousNumOfRounds;
        newStartingPlayer = previousStartingPlayer;
        setNewSoundSwitch(previousSoundSwitch);
        newSound = previousSound;

        // init default toggled
        defaultToggled = false;
    }

    public void setNewPlayerMode(String PlayerMode) {
        this.newPlayerMode = PlayerMode;
        // local class
        class Local {
            private void setRadioButtonEnable(boolean state){
                RadioButton myRadioButton;
                // setEnable() to true or false for view
                myRadioButton = (RadioButton) findViewById(R.id.low_radio_button);
                myRadioButton.setEnabled(state);
                // setEnable() to true or false for view
                myRadioButton = (RadioButton) findViewById(R.id.mid_radio_button);
                myRadioButton.setEnabled(state);
                // setEnable() to true or false view
                myRadioButton = (RadioButton) findViewById(R.id.high_radio_button);
                myRadioButton.setEnabled(state);
            }
        }
        // Declare local instance
        Local myLocalDiff = new Local();
        if (newPlayerMode.equals(TWO_PLAYERS)) {
            myLocalDiff.setRadioButtonEnable(false);
        }
        else {
            myLocalDiff.setRadioButtonEnable(true);
        }
    }

    // Set soundSwitch via set method so that the SoundGroup can be enabled/disabled depending on
    // whether soundSwitch is ON or OFF
    public void setNewSoundSwitch(boolean soundSwitch) {
        this.newSoundSwitch = soundSwitch;
        // Local class
        class Local {
            private void setRadioButtonEnable(boolean state){
                RadioButton myRadioButton;
                // setEnable() to true or false view
                myRadioButton = (RadioButton) findViewById(R.id.buzz_sound_radio_button);
                myRadioButton.setEnabled(state);
                // setEnable() to true or false view
                myRadioButton = (RadioButton) findViewById(R.id.bell_sound_radio_button);
                myRadioButton.setEnabled(state);
                // setEnable() to true or false view
                myRadioButton = (RadioButton) findViewById(R.id.xmas_sound_radio_button);
                myRadioButton.setEnabled(state);
            }
        }
        // Declare local instance
        Local myLocalSound = new Local();
        if (newSoundSwitch) {
            myLocalSound.setRadioButtonEnable(true);
        }
        else {
            myLocalSound.setRadioButtonEnable(false);
        }
    }



    @Override
    protected void onStart() {
        super.onStart();
        // This should be called after decipherIncomingIntent() has been actioned in onCreate(),
        // this ensures that the newSettings have been assigned the previousSettings, this approach
        // ensures that the checked() buttons are also of the updated values throughout User Interaction
        setRadioButtonCheckedStatus();
    }

    // TODO - retrieve current setting as sent by incoming Intent and set this as previous
    //  within this activity.
    private void decipherIncomingIntent(){
        // Retrieve intent
        Intent incomingIntent = getIntent();

        // Retrieve intent Extras
        this.previousPlayerMode = incomingIntent.getStringExtra(EXTRA_PLAYER_MODE);
        this.previousDifficulty = incomingIntent.getStringExtra(EXTRA_DIFFICULTY);
        this.previousNumOfRounds = incomingIntent.getIntExtra(EXTRA_ROUND_NUM, 1);
        this.previousStartingPlayer = incomingIntent.getStringExtra(EXTRA_STARTING_PLAYER);
        this.previousSoundSwitch = incomingIntent.getBooleanExtra(EXTRA_SOUND_SWITCH, false);
        this.previousSound = incomingIntent.getStringExtra(EXTRA_SOUND);
    }


    /***
     * ===================================== RADIO BUTTONS ======================================
     */
    //Check current settings on Radio Buttons and check according to indicate these to User
    private void setRadioButtonCheckedStatus() {
        // Declare a method global RadioGroup var.
        RadioGroup myRadioGroup;

        // Indicate the current playerMode settings
        myRadioGroup = findViewById(R.id.player_mode_radio_group);
        if (newPlayerMode.equals(TWO_PLAYERS)) {
            myRadioGroup.check(R.id.two_player_radio_button);
        }
        else {
            myRadioGroup.check(R.id.one_player_radio_button);
        }

        // Indicate the current difficulty setting
        myRadioGroup = findViewById(R.id.difficulty_radio_group);
        if (newDifficulty.equals(LOW_DIFFICULTY)) {
            myRadioGroup.check(R.id.low_radio_button);
        }
        else if (newDifficulty.equals(MID_DIFFICULTY)){
            myRadioGroup.check(R.id.mid_radio_button);
        }
        else {
            myRadioGroup.check(R.id.high_radio_button);
        }

        // Indicate the current StartingPlayer setting
        myRadioGroup = findViewById(R.id.starting_player_radio_group);
        if (newStartingPlayer.equals(PLAYER_X)) {
            myRadioGroup.check(R.id.player_x_radio_button);
        }
        else if (newStartingPlayer.equals(PLAYER_O)) {
            myRadioGroup.check(R.id.player_o_radio_button);
        }
        else {
            // it must be rand
            myRadioGroup.check(R.id.random_radio_button);
        }

        // Indicate the current Rounds setting
        myRadioGroup = findViewById(R.id.rounds_radio_group);
        if (newNumOfRounds == ONE_ROUND) {
            myRadioGroup.check(R.id.one_round_radio_button);
        }
        else if (newNumOfRounds == THREE_ROUND) {
            myRadioGroup.check(R.id.three_round_radio_button);
        }
        else if (newNumOfRounds == SEVEN_ROUND) {
            myRadioGroup.check(R.id.seven_round_radio_button);
        }
        else {
            myRadioGroup.check(R.id.eleven_round_radio_button);
        }

        // Indicate the current Sound setting
        // SoundSwitch
        Switch mySwitch = findViewById(R.id.sound_switch);
        if ( newSoundSwitch ){
            mySwitch.setChecked(true);
        }
        else {
            mySwitch.setChecked(false);
        }
        // RadioGroup
        myRadioGroup = findViewById(R.id.sound_radio_group);
        if ( newSound.equals(BUZZ_SOUND) ) {
            myRadioGroup.check(R.id.buzz_sound_radio_button);
        }
        else if (newSound.equals(BELL_SOUND)){
            myRadioGroup.check(R.id.bell_sound_radio_button);
        }
        else {
            // current sound selection must be xmas
            myRadioGroup.check(R.id.xmas_sound_radio_button);
        }
    }


    /**
     * Handling PlayerMode RadioButtonGroup
      */
    public void onPlayerModeRadioButtonClicked(View view) {
        RadioButton difficultyRG = (RadioButton) findViewById(R.id.low_radio_button);
        // Test default condition (and allow if appropriate its activation)
        setDefaultToggled();
        // Is the button now checked?
        boolean checked = ((RadioButton) view).isChecked();
        // Check which radio button was clicked
        switch(view.getId()) {
            case R.id.one_player_radio_button:
                if (checked) {
                    // Update
                    setNewPlayerMode(ONE_PLAYER);
                    break;
                }
            case R.id.two_player_radio_button:
                if (checked) {
                    // Update accordingly
                    setNewPlayerMode(TWO_PLAYERS);
                    break;
                }
        }
    }

    // Handling Difficulty RadioButtonGroup
    public void onDifficultyRadioButtonClicked(View view) {
        // Test default condition (and allow if appropriate its activation)
        setDefaultToggled();

        // Is the button now checked?
        boolean checked = ((RadioButton) view).isChecked();
        // Check which radio button was clicked
        switch(view.getId()) {
            case R.id.low_radio_button:
                if (checked)
                    // Update
                    newDifficulty = LOW_DIFFICULTY;
                break;
            case R.id.mid_radio_button:
                if (checked)
                    // Update accordingly
                    newDifficulty = MID_DIFFICULTY;
                break;
            case R.id.high_radio_button:
                if (checked)
                    // Update accordingly
                    newDifficulty = HIGH_DIFFICULTY;
                break;
        }
    }

    // Handling Starter RadioButtonGroup
    public void onStarterRadioButtonClicked(View view) {
        // Test default condition (and allow if appropriate its activation)
        setDefaultToggled();

        // Is the button now checked?
        boolean checked = ((RadioButton) view).isChecked();
        // Check which radio button was clicked
        switch(view.getId()) {
            case R.id.player_x_radio_button:
                if (checked)
                    // Update
                    newStartingPlayer = PLAYER_X;
                break;
            case R.id.player_o_radio_button:
                if (checked)
                    // Update accordingly
                    newStartingPlayer = PLAYER_O;
                break;
            case R.id.random_radio_button:
                if (checked)
                    // Update accordingly
                    newStartingPlayer = RANDOM_START;
                break;
        }
    }

    // Handling Rounds RadioButtonGroup
    public void onRoundsRadioButtonClicked(View view) {
        // Test default condition (and allow if appropriate its activation)
        setDefaultToggled();

        // Is the button now checked?
        boolean checked = ((RadioButton) view).isChecked();
        // Check which radio button was clicked
        switch(view.getId()) {
            case R.id.one_round_radio_button:
                if (checked)
                    // Update
                    newNumOfRounds = ONE_ROUND;
                break;
            case R.id.three_round_radio_button:
                if (checked)
                    // Update accordingly
                    newNumOfRounds = THREE_ROUND;
                break;
            case R.id.seven_round_radio_button:
                if (checked)
                    // Update accordingly
                    newNumOfRounds = SEVEN_ROUND;
                break;
            case R.id.eleven_round_radio_button:
                if (checked)
                    // Update accordingly
                    newNumOfRounds = ELEVEN_ROUND;
                break;
        }
    }

    // Handling Sounds RadioButton
    // SoundSwitch
    public void onSoundSwitchClick(View view){
        Switch soundSwitch = findViewById(R.id.sound_switch);

        // Is the switch checked?
        boolean checked = ((Switch) view).isChecked();
        if (checked){
            // Update
            setNewSoundSwitch(SOUND_ON);
        }
        else {
            setNewSoundSwitch(SOUND_OFF);
        }
    }

    // Handling Starter RadioButtonGroup
    public void onSoundRadioButtonClicked(View view) {
        // Test default condition (and allow if appropriate its activation)
        setDefaultToggled();

        // Is the button now checked?
        boolean checked = ((RadioButton) view).isChecked();
        // Check which radio button was clicked
        switch(view.getId()) {
            case R.id.buzz_sound_radio_button:
                if (checked)
                    // Update
                    newSound = BUZZ_SOUND;
                break;
            case R.id.bell_sound_radio_button:
                if (checked)
                    // Update accordingly
                    newSound = BELL_SOUND;
                break;
            case R.id.xmas_sound_radio_button:
                if (checked)
                    // Update accordingly
                    newSound = XMAS_SOUND;
                break;
        }
    }

    // TODO - On user pressing on default, check all the default options to display to user what
    //  the defaults are and ensure that the respective values are simultaneously written to
    //  variables
    public void defaultButtonOnClick(View view) {
        // Ensures that default is not sporadically written to, pressable again if user alters
        // selection through the other on click options
        if ( !defaultToggled ) {
            setNewPlayerMode(TWO_PLAYERS);
            newDifficulty = MID_DIFFICULTY;
            newStartingPlayer = RANDOM_START;
            newNumOfRounds = ONE_ROUND;
            setNewSoundSwitch(SOUND_OFF);
            newSound = BUZZ_SOUND;
            // update checked radio buttons
            setRadioButtonCheckedStatus();
            // set to true so that user cannot repeatedly write to the variable when naught
            // has changed.
            defaultToggled = true;
        }
    }

    private void setDefaultToggled() {
        // TODO - Make sure that the if statement condition which reflects the DEFAULT SETTINGS are
        //  updated to represent the adopted default values as project progresses.

        // Ensures that after the default button is pressed, it is only pressable again if the
        // checked values divert from the prescribed default
        if (  defaultToggled && !( newPlayerMode.equals(TWO_PLAYERS) &&
                newDifficulty.equals(MID_DIFFICULTY) && newStartingPlayer.equals(RANDOM_START) &&
                newNumOfRounds==ONE_ROUND && newSound.equals(BUZZ_SOUND) )  ) {
            defaultToggled = false;
        }
    }

    //TODO - EVALUATE FURTHER THIS SECTION BEFORE CODE TESTING AND ASLO MAkE A FLOWCHART ON WORD!!

    // Set the action to take for setting return Intent to Main depending on whether Cancel,
    // Default or Accept is pressed.
    public void setActivityResultOnClick(View view) {
        Intent returnIntent = new Intent("");

        if ( view.getId() == R.id.accept_button ){
            // User made changes (or adopted default settings) and pressed accept button.
            returnIntent.putExtra(EXTRA_PLAYER_MODE, newPlayerMode)
                    .putExtra(EXTRA_DIFFICULTY, newDifficulty)
                    .putExtra(EXTRA_STARTING_PLAYER, newStartingPlayer)
                    .putExtra(EXTRA_ROUND_NUM, newNumOfRounds)
                    .putExtra(EXTRA_SOUND_SWITCH, newSoundSwitch)
                    .putExtra(EXTRA_SOUND, newSound);
            this.setResult(Activity.RESULT_OK, returnIntent);
        }
        else {
            this.setResult( Activity.RESULT_CANCELED,
                    returnIntent.putExtra("reason", "Settings process cancelled.")
                            .putExtra(EXTRA_PLAYER_MODE, previousPlayerMode)
                            .putExtra(EXTRA_DIFFICULTY, previousDifficulty)
                            .putExtra(EXTRA_STARTING_PLAYER, previousStartingPlayer)
                            .putExtra(EXTRA_ROUND_NUM, previousNumOfRounds)
                            .putExtra(EXTRA_SOUND_SWITCH, previousSoundSwitch)
                            .putExtra(EXTRA_SOUND, previousSound) );
        }

        // Finish Activity so that results may be returned to MainActivity
        SettingsFullScreenActivity.this.finish();
    }


}
