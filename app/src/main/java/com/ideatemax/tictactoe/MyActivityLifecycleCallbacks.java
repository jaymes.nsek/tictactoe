/*
 * Copyright (c) 2020. Written by James Nsek
 */

package com.ideatemax.tictactoe;import android.app.Activity;
import android.app.Application;
import android.os.Bundle;
import android.util.Log;

public class MyActivityLifecycleCallbacks implements Application.ActivityLifecycleCallbacks {

    private Activity currentActivity;

    @Override
    public void onActivityCreated(Activity activity, Bundle savedInstanceState) {
        currentActivity = activity;

        Log.i(activity.getClass().getSimpleName(), "onCreate()");
    }

    @Override
    public void onActivityStarted(Activity activity) {
        currentActivity = activity;

        Log.i(activity.getClass().getSimpleName(), "onStart()");
    }

    @Override
    public void onActivityResumed(Activity activity) {
        currentActivity = activity;

        Log.i(activity.getClass().getSimpleName(), "onResume()");
    }

    @Override
    public void onActivityPaused(Activity activity) {
        currentActivity = activity;

        Log.i(activity.getClass().getSimpleName(), "onPause()");
    }

    @Override
    public void onActivityStopped(Activity activity) {
        Log.i(activity.getClass().getSimpleName(), "onStop()");
    }

    @Override
    public void onActivitySaveInstanceState(Activity activity, Bundle outState) {
        Log.i(activity.getClass().getSimpleName(), "onSaveInstanceState()");
    }

    @Override
    public void onActivityDestroyed(Activity activity) {
        Log.i(activity.getClass().getSimpleName(), "onDestroy()");
    }

    Activity getCurrentActivity() {
        return currentActivity;
    }
}
