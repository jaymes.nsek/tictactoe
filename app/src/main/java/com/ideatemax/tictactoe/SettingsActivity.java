/*
 * Copyright (c) 2020. Written by James Nsek
 */

package com.ideatemax.tictactoe;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;

import androidx.activity.OnBackPressedCallback;

import static com.ideatemax.tictactoe.GameStats.Difficulty.HIGH;
import static com.ideatemax.tictactoe.GameStats.Difficulty.LOW;
import static com.ideatemax.tictactoe.GameStats.Difficulty.MID;
import static com.ideatemax.tictactoe.GameStats.FirstMover.RANDOM;
import static com.ideatemax.tictactoe.GameStats.PlayerMode.ONE;
import static com.ideatemax.tictactoe.GameStats.PlayerMode.TWO;
import static com.ideatemax.tictactoe.MainActivity.ELEVEN_ROUND;
import static com.ideatemax.tictactoe.MainActivity.EXTRA_DIFFICULTY;
import static com.ideatemax.tictactoe.MainActivity.EXTRA_O_NAME;
import static com.ideatemax.tictactoe.MainActivity.EXTRA_PLAYER_MODE;
import static com.ideatemax.tictactoe.MainActivity.EXTRA_TOTAL_ROUNDS;
import static com.ideatemax.tictactoe.MainActivity.EXTRA_FIRST_MOVER;
import static com.ideatemax.tictactoe.MainActivity.EXTRA_X_NAME;
import static com.ideatemax.tictactoe.MainActivity.ONE_ROUND;
import static com.ideatemax.tictactoe.MainActivity.SEVEN_ROUND;
import static com.ideatemax.tictactoe.MainActivity.THREE_ROUND;
import static com.ideatemax.tictactoe.Player.PHONE_O_NAME;
import static com.ideatemax.tictactoe.Player.O_SYM;
import static com.ideatemax.tictactoe.Player.PLAYER_O_NAME;
import static com.ideatemax.tictactoe.Player.X_SYM;
import static com.ideatemax.tictactoe.Player.PLAYER_X_NAME;


public class SettingsActivity extends LifecycleLoggingActivity {

    // FIELDS/VARIABLES

    // Previous settings.
    private String previousPlayerMode;
    private String previousDifficulty;
    private int previousNumOfRounds;
    private String previousFirstMover;
    private String previousNameX;
    private String previousNameO;

    // New settings
    private String newPlayerMode;
    private String newDifficulty;
    private int newNumOfRounds;
    private String newFirstMover;
    private String newNameX;
    private String newNameO;

    // Declare local EditText.
    private EditText xNameEditText;
    private EditText oNameEditText;

    // Text Size Dynamically
    private boolean textSizeComputed;

    // Declare an Array of TextView by which to iterate over and update their sizes
    private TextView[] titleTextViews;
    private TextView[] bodyTextViews;
    private EditText[] bodyEditText;
    private RadioButton[] bodyRadioButton;
    private Button[] dialogButtons;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_settings);

        // Handle onBackPressed
        // This callback will only be called when SettingsActivity is at least Started.
        OnBackPressedCallback callback =
                new OnBackPressedCallback(true /* enabled by default */) {
            @Override
            public void handleOnBackPressed() {
                // Handle the back button event
                View cancelButton = findViewById(R.id.cancel_button);
                setActivityResultOnClick(cancelButton);
            }
        };
        this.getOnBackPressedDispatcher().addCallback(this, callback);

        // The callback can be enabled or disabled here or in handleOnBackPressed()

        // Retrieve incoming intent extras
        decipherIncomingIntent();

        // Assign previous setting to current, so that if user cancels the process we may simple
        // reinstate it
        setNewPlayerMode(previousPlayerMode);
        newDifficulty = previousDifficulty;
        newNumOfRounds = previousNumOfRounds;
        newFirstMover = previousFirstMover;
        newNameX = previousNameX;
        newNameO = previousNameO;

        // set onEditorListener
        /*setEnterListener();*/

        // manage textSizes
        cacheViews();

        // Maximise text size to height constrain
        setTextSize();
    }


    @Override
    protected void onStart() {
        super.onStart();
        // This should be called after decipherIncomingIntent() has been actioned in onCreate(),
        // this ensures that the newSettings have been assigned the previousSettings, this approach
        // ensures that the checked() buttons are also of the updated values throughout
        // User Interaction
        mapPreviousSettingsToUser();
    }


    /**
     * Method sets textSizes based on device metrics. This approach was adopted as target minimum
     * API is 22 and does not support xml autoText attribute.
     */
    private void setTextSize(){
        if (!textSizeComputed){
            // Retrieve device specific data.
            float scaled_density = Utils.getScaled_density(this);
            int widthPixels = Utils.getWidth_inPixels(this);
            int heightPixels = Utils.getHeight_inPixels(this);

            // Compute textSizes from device specific values.
            float textSize_pixels = (float)  (heightPixels/76) ;
            float textSizeBody_pixels = (float) ( textSize_pixels);

            // SettingAction (Cancel - default - Apply) Buttons
            float margins = ( (32+8)*2 ) * scaled_density;

            float buttonSpacing = (8*6) * scaled_density;

            float actionButtonsTextSize = ((widthPixels -
                    (margins + buttonSpacing))/3)/ (float)7.0;

            float dp_val = (actionButtonsTextSize/scaled_density);

            // Adapt the textSize to a 2dp scale factor

            // set textSizes programmatically
            for (TextView view : titleTextViews) {
                // (float)(48 * 2.51)
                view.setTextSize( textSize_pixels );
            }
            for (TextView view : bodyTextViews) {
                // (float)(48 * 2.51)
                view.setTextSize(textSizeBody_pixels);
            }
            for (EditText view : bodyEditText) {
                // (float)(48 * 2.51)
                view.setTextSize(textSizeBody_pixels);
            }
            for (RadioButton view : bodyRadioButton) {
                // (float)(48 * 2.51)
                view.setTextSize(textSizeBody_pixels);
            }
            for (Button view : dialogButtons) {
                // (float)(48 * 2.51)
                view.setTextSize(dp_val);
            }

            // flag textSize as computed
            textSizeComputed = true;
        }
    }


    /**
     * Cache Views so that they are close by for later use.
     */
    public void cacheViews(){
        // Init View Objects.
        titleTextViews = new TextView[5];
        bodyTextViews = new TextView[2];
        bodyEditText = new EditText[2];
        bodyRadioButton = new RadioButton[12];
        dialogButtons = new Button[3];

        // Title TextViews
        titleTextViews[0] = findViewById(R.id.naming_title);
        titleTextViews[1] = findViewById(R.id.player_mode_title);
        titleTextViews[2] = findViewById(R.id.difficulty_mode_title);
        titleTextViews[3] = findViewById(R.id.first_move_title);
        titleTextViews[4] = findViewById(R.id.round_title);

        // Body text views
        bodyTextViews[0] = findViewById(R.id.x_name);
        bodyTextViews[1] = findViewById(R.id.o_name);

        // Body EditText
        bodyEditText[0] = findViewById(R.id.x_enter_name);
        bodyEditText[1] = findViewById(R.id.o_enter_name);

        // Body RadioButtons
        bodyRadioButton[0] = findViewById(R.id.radio_1p_btn);
        bodyRadioButton[1] = findViewById(R.id.radio_2p_btn);
        bodyRadioButton[2] = findViewById(R.id.low_radio_btn);
        bodyRadioButton[3] = findViewById(R.id.mid_radio_btn);
        bodyRadioButton[4] = findViewById(R.id.high_radio_btn);
        bodyRadioButton[5] = findViewById(R.id.x_first_move_rbtn);
        bodyRadioButton[6] = findViewById(R.id.o_first_move_rbtn);
        bodyRadioButton[7] = findViewById(R.id.rand_first_move_rbtn);
        bodyRadioButton[8] = findViewById(R.id.one_rnd_rbtn);
        bodyRadioButton[9] = findViewById(R.id.three_rnd_rbtn);
        bodyRadioButton[10] = findViewById(R.id.seven_rnd_rbtn);
        bodyRadioButton[11] = findViewById(R.id.eleven_rnd_rbtn);

        // Dialog buttons
        dialogButtons[0] = findViewById(R.id.default_button);
        dialogButtons[1] = findViewById(R.id.cancel_button);
        dialogButtons[2] = findViewById(R.id.accept_button);
    }


    /**
     * Retrieve current setting as sent by incoming Intent and set this as previous
     *  within this activity.
     */
    private void decipherIncomingIntent(){
        // Retrieve intent
        Intent incomingIntent = getIntent();

        // Retrieve intent Extras
        this.previousPlayerMode = incomingIntent.getStringExtra(EXTRA_PLAYER_MODE);

        this.previousDifficulty = incomingIntent.getStringExtra(EXTRA_DIFFICULTY);

        this.previousNumOfRounds = incomingIntent.getIntExtra(EXTRA_TOTAL_ROUNDS, 1);

        this.previousFirstMover = incomingIntent.getStringExtra(EXTRA_FIRST_MOVER);

        this.previousNameX = incomingIntent.getStringExtra(EXTRA_X_NAME);

        this.previousNameO = incomingIntent.getStringExtra(EXTRA_O_NAME);
    }


    /**
     * Used to toggle difficulty selection between enable and disabled mode based on playerMode.
     * @param PlayerMode one or two player mode.
     */
    public void setNewPlayerMode(String PlayerMode) {
        this.newPlayerMode = PlayerMode;

        // local class such that the method is within the method, since its only used here.
        class Local {
            private void setRadioButtonEnable(boolean state){
                RadioButton myRadioButton;

                // setEnable() to true or false for view
                myRadioButton = (RadioButton) findViewById(R.id.low_radio_btn);
                myRadioButton.setEnabled(state);

                // setEnable() to true or false for view
                myRadioButton = (RadioButton) findViewById(R.id.mid_radio_btn);
                myRadioButton.setEnabled(state);

                // setEnable() to true or false view
                myRadioButton = (RadioButton) findViewById(R.id.high_radio_btn);
                myRadioButton.setEnabled(state);
            }
        }

        // Declare local EditText object and assign name TextView to it.
        EditText editText;
        editText = findViewById(R.id.o_enter_name);

        // Init local class for gaining access to its method.
        Local myLocalDiff = new Local();

        // When in TWO_PLAYER mode, disable RadioGroup selection.
        if (newPlayerMode.equals(TWO.toString())) {
            // disable radio group.
            myLocalDiff.setRadioButtonEnable(false);

            // set Player O hint as "Player O"
            editText.setHint("Player O");
        }
        // if in single player mode
        else {
            // enable radio group.
            myLocalDiff.setRadioButtonEnable(true);

            // set Player O hint as "Computer"
            editText.setHint("Phone O");
        }
    }



    // --------------------------------- RADIO BUTTONS ---------------------------------------

    /**
     * Check current settings on Radio Buttons and check according to indicate these to User.
     */
    private void mapPreviousSettingsToUser() {
        // Declare a global objects to adopt within this method.
        TextView myTextView;
        RadioGroup myRadioGroup;
        String name;
        int id;

        // Retrieve View for playerX name.
        myTextView = findViewById(R.id.x_enter_name);
        // Indicate the current player name for X. If TRUE clear textView - Hint should show, else
        // if FALSE the a unique name was entered by user, apply this.
        name = newNameX.equals(PLAYER_X_NAME) ? "" : newNameX;
        myTextView.setText(name);

        // Retrieve View for playerO name.
        myTextView = findViewById(R.id.o_enter_name);
        // Clear current player name for O if TRUE. If FALSE a unique name apart from the default
        // constructed name is entered, apply it.
        name = (newNameO.equals(PLAYER_O_NAME) || newNameO.equals(PHONE_O_NAME)) ? "" : newNameO;
        myTextView.setText(name);

        // Retrieve View for player mode RadioGroup.
        myRadioGroup = findViewById(R.id.player_mode_radio_group);
        // Indicate the current setting for playerMode .
        id = newPlayerMode.equals(TWO.toString()) ? R.id.radio_2p_btn : R.id.radio_1p_btn;
        myRadioGroup.check(id);

        // Retrieve View for diff RadioGroup.
        myRadioGroup = findViewById(R.id.difficulty_radio_group);
        // Indicate the current difficulty setting
        id = newDifficulty.equals(LOW.toString()) ? R.id.low_radio_btn :
                newDifficulty.equals(MID.toString()) ? R.id.mid_radio_btn : R.id.high_radio_btn;
        myRadioGroup.check(id);

        // Retrieve View for starting player RadioGroup.
        myRadioGroup = findViewById(R.id.first_mover_radio_group);
        // Indicate the current FirstMover setting.
        id = newFirstMover.equals(X_SYM) ? R.id.x_first_move_rbtn :
                newFirstMover.equals(O_SYM) ? R.id.o_first_move_rbtn : R.id.rand_first_move_rbtn;
        myRadioGroup.check(id);


        // Retrieve View for rounds RadioGroup.
        myRadioGroup = findViewById(R.id.rounds_radio_group);
        // Indicate the current Rounds setting.
        id = (newNumOfRounds == ONE_ROUND) ? R.id.one_rnd_rbtn :
                newNumOfRounds == THREE_ROUND ? R.id.three_rnd_rbtn :
                        newNumOfRounds == SEVEN_ROUND ? R.id.seven_rnd_rbtn : R.id.eleven_rnd_rbtn;
        myRadioGroup.check(id);
    }


    /**
     * Handle PlayerMode onClicked event.
     * @param view clicked View obj.
      */
    public void onPlayerModeRadioButtonClicked(View view) {
        // Is the button now checked?
        boolean checked = ((RadioButton) view).isChecked();

        // Check which radio button was clicked
        switch(view.getId()) {
            case R.id.radio_1p_btn:
                if (checked) {
                    // Update
                    setNewPlayerMode(ONE.toString());
                    break;
                }
            case R.id.radio_2p_btn:
                if (checked) {
                    // Update accordingly
                    setNewPlayerMode(TWO.toString());
                    break;
                }
        }
    }


    /**
     * Handle Difficulty onClicked event.
     * @param view clicked View obj.
     */
    public void onDifficultyRadioButtonClicked(View view) {
        // Is the button now checked?
        boolean checked = ((RadioButton) view).isChecked();
        // Check which radio button was clicked
        switch(view.getId()) {
            case R.id.low_radio_btn:
                if (checked)
                    // Update
                    newDifficulty = LOW.toString();
                break;
            case R.id.mid_radio_btn:
                if (checked)
                    // Update accordingly
                    newDifficulty = MID.toString();
                break;
            case R.id.high_radio_btn:
                if (checked)
                    // Update accordingly
                    newDifficulty = HIGH.toString();
                break;
        }
    }


    /**
     *  Handle FirstMover onClicked event.
     * @param view clicked View obj.
     */
    public void onFirstMoverButtonClicked(View view) {
        // Is the button now checked?
        boolean checked = ((RadioButton) view).isChecked();

        // Check which radio button was clicked
        switch(view.getId()) {
            case R.id.x_first_move_rbtn:
                if (checked)
                    // Update
                    newFirstMover = X_SYM;
                break;
            case R.id.o_first_move_rbtn:
                if (checked)
                    // Update accordingly
                    newFirstMover = O_SYM;
                break;
            case R.id.rand_first_move_rbtn:
                if (checked)
                    // Update accordingly
                    newFirstMover = RANDOM.toString();
                break;
        }
    }


    /**
     * Handle rounds onClicked event.
     * @param view clicked View obj.
     */
    public void onRoundsRadioButtonClicked(View view) {
        // Is the button now checked?
        boolean checked = ((RadioButton) view).isChecked();

        // Check which radio button was clicked
        switch(view.getId()) {
            case R.id.one_rnd_rbtn:
                if (checked)
                    // Update
                    newNumOfRounds = ONE_ROUND;
                break;
            case R.id.three_rnd_rbtn:
                if (checked)
                    // Update accordingly
                    newNumOfRounds = THREE_ROUND;
                break;
            case R.id.seven_rnd_rbtn:
                if (checked)
                    // Update accordingly
                    newNumOfRounds = SEVEN_ROUND;
                break;
            case R.id.eleven_rnd_rbtn:
                if (checked)
                    // Update accordingly
                    newNumOfRounds = ELEVEN_ROUND;
                break;
        }
    }


    /**
     * Updates player names values specified by user.
     */
    private void updateNames() {
        // Declare local objects for local assignment.
        TextView textView;
        String text;

        // Init playerX textView.
        textView = findViewById(R.id.x_enter_name);
        // Retrieve textView value.
        text = textView.getText().toString();
        // Check to see if user entered data in nameX TextView, update if TRUE.
        newNameX = text.equals("") ? PLAYER_X_NAME : textView.getText().toString();


        // Init playerX textView.
        textView = findViewById(R.id.o_enter_name);
        // Retrieve textView value.
        text = textView.getText().toString();
        // Check if no data is entered playerO TextView -- when FALSE set provided name - when TRUE
        // jump to two subChoice depending on whether in 1P mode or not --set this an dont .
        newNameO = text.equals("") ?
                newPlayerMode.equals(ONE.toString()) ? PHONE_O_NAME : PLAYER_O_NAME
                : text;
    }


    /**
     * On user pressing on default, MARK all the default options to display to user what the
     * defaults are and that the respective values are simultaneously written as new variables.
     * @param view default view Button.
     */
    public void defaultButtonOnClick(View view) {
        // Enforce default values.
        setNewPlayerMode(ONE.toString());
        newDifficulty = MID.toString();
        newFirstMover = RANDOM.toString();
        newNumOfRounds = THREE_ROUND;
        newNameX = "";
        newNameO = "";

        // update checked radio buttons
        mapPreviousSettingsToUser();
    }


    /**
     * Set the action to take for setting return Intent to Main depending on whether Cancel,
     * Default, or Apply button is pressed.
     * @param view Default, Cancel, or Apply view.
     */
    public void setActivityResultOnClick(View view) {
        // Update newly entered names.
        updateNames();

        // set intent return results.
        Intent returnIntent = new Intent("");

        if ( view.getId() == R.id.accept_button ){
            // User made changes (or adopted default settings) and pressed accept button.
            returnIntent.putExtra(EXTRA_PLAYER_MODE, newPlayerMode)
                    .putExtra(EXTRA_DIFFICULTY, newDifficulty)
                    .putExtra(EXTRA_FIRST_MOVER, newFirstMover)
                    .putExtra(EXTRA_TOTAL_ROUNDS, newNumOfRounds)
                    .putExtra(EXTRA_X_NAME, newNameX)
                    .putExtra(EXTRA_O_NAME, newNameO);
            //.putExtra(EXTRA_SOUND_SWITCH, newSoundSwitch)
            // Set result to return.
            this.setResult(Activity.RESULT_OK, returnIntent);
        }
        else {
            returnIntent.putExtra("reason", "Settings process cancelled.")
                    .putExtra(EXTRA_PLAYER_MODE, previousPlayerMode)
                    .putExtra(EXTRA_DIFFICULTY, previousDifficulty)
                    .putExtra(EXTRA_FIRST_MOVER, previousFirstMover)
                    .putExtra(EXTRA_TOTAL_ROUNDS, previousNumOfRounds)
                    .putExtra(EXTRA_X_NAME, previousNameX)
                    .putExtra(EXTRA_O_NAME, previousNameO);
            //.putExtra(EXTRA_SOUND_SWITCH, previousSoundSwitch)
            // Set result to return.
            this.setResult( Activity.RESULT_CANCELED, returnIntent);
        }

        // Finish Activity so that results may be returned to MainActivity
        SettingsActivity.this.finish();
    }


    /**
     * Manage TextView Listeners.
     */
    /*private void setEnterListener() {
        // Cache Views.
        xNameEditText = (EditText) findViewById(R.id.x_enter_name);
        oNameEditText = (EditText) findViewById(R.id.o_enter_name);

        // Register a listener to accept user input when the Enter key is pressed.
        oNameEditText.setOnEditorActionListener
                (new TextView.OnEditorActionListener() {
                    @Override
                    public boolean onEditorAction(TextView textView, int actionId, KeyEvent event) {
                        boolean handled = false;
                        if (actionId == EditorInfo.IME_NULL ||
                                actionId == EditorInfo.IME_ACTION_SEARCH ||
                                actionId == EditorInfo.IME_ACTION_DONE ||
                                event.getAction() == KeyEvent.ACTION_DOWN &&
                                        event.getKeyCode() == KeyEvent.KEYCODE_ENTER)
                        {
                            handled = true;
                            textView.clearFocus();
                            Utils.hideKeyboard(SettingsActivity.this,
                                    oNameEditText.getWindowToken());
                        }
                        return  handled;
                    }
                });
    }*/
}
