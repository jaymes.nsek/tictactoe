/*
 * Copyright (c) 2020. Written by James Nsek
 */

package com.ideatemax.tictactoe;


import android.content.Context;
import android.content.Intent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageButton;

import java.util.List;

import static com.ideatemax.tictactoe.MainActivity.EXTRA_DIFFICULTY;
import static com.ideatemax.tictactoe.MainActivity.EXTRA_FIRST_MOVER;
import static com.ideatemax.tictactoe.MainActivity.EXTRA_O_NAME;
import static com.ideatemax.tictactoe.MainActivity.EXTRA_PLAYER_MODE;
import static com.ideatemax.tictactoe.MainActivity.EXTRA_TOTAL_ROUNDS;
import static com.ideatemax.tictactoe.MainActivity.EXTRA_X_NAME;

public class Utils {

    // FIELDS

    // Logging tag
    private static final  String TAG = "Utils";

    public static WinCase[] winCases = new WinCase[8];

    // the height and weight dimension for achieving a square grid whilst utilizing screen space
    public static int gridSize;
    public static int gridGroupWidth;

    // Flagging variables for use in MainActivity and ComputerMoveManger classes.
    public static final int WIN_FLAG = 100;
    public static final int BLOCK_FLAG = 50;


    // Private constructor to prevent use of class instantiation, IOW use of "new".
    private Utils() {
        throw new AssertionError();
    }


    /**
     * When method is called, get the width dp and set the height equal to it
     * @param view view object to enforce size.
     * @param gridSize the size val to apply.
     */
    public static void setWidthDynamically(View view, int gridSize){
        // Cast the view.
        Button thisView = (Button) view;

        //Init mParams to 0,0 since the constructor needs values
        ViewGroup.LayoutParams mParams = thisView.getLayoutParams();

        //set the width and height using the passed gridSize value
        mParams.width = gridSize;
        mParams.height = gridSize;

        //set the dimensions
        thisView.setLayoutParams(mParams);
    }


    /**
     * Initialises win cases.
     */
    public static void initialiseWinCases(){
        winCases[0] = new WinCase("a1", "a2", "a3");
        winCases[1] = new WinCase("a1", "b1", "c1");
        winCases[2] = new WinCase("a1", "b2", "c3");
        winCases[3] = new WinCase("a2", "b2", "c2");
        winCases[4] = new WinCase("a3", "b3", "c3");
        winCases[5] = new WinCase("b1", "b2", "b3");
        winCases[6] = new WinCase("a3", "b2", "c1");
        winCases[7] = new WinCase("c1", "c2", "c3");
    }


    /**
     * Copies held grid coordinates to a List object.
     * @param gridList which the default playable coordinates are loaded to.
     */
    public static void initialiseGridCoordinates(List<String> gridList){
        // Enforce HMap is empty
        gridList.clear();

        // set Grid Coordinates
        gridList.add("a1");
        gridList.add("a2");
        gridList.add("a3");
        gridList.add("b1");
        gridList.add("b2");
        gridList.add("b3");
        gridList.add("c1");
        gridList.add("c2");
        gridList.add("c3");
    }


    /**
     * Method extrapolates device specific Height.
     * @param context AppContext of initiating Activity.
     * @return device height.
     */
    public static int getHeight_inPixels(Context context) {
        return context.getResources().getDisplayMetrics().heightPixels;
    }


    /**
     * Method extrapolates device specific Width.
     * @param context AppContext of initiating Activity.
     * @return device width.
     */
    public static int getWidth_inPixels(Context context) {
        return context.getResources().getDisplayMetrics().widthPixels;
    }


    /**
     * Method extrapolates device specific scale_density.
     * @param context AppContext of initiating Activity.
     * @return device scale_density.
     */
    public static float getScaled_density(Context context) {
        return context.getResources().getDisplayMetrics().scaledDensity;
    }


    /**
     * Handles a share intent.
     * @param imageBtn is the object to set listener on.
     */
    public static void setOnShareClickedListener(final Context context, ImageButton imageBtn) {
        imageBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String subject = "The-TicTacToe Game";

                String body = "Heads-up!, you should consider downloading this #awesome " +
                        "TicTacToe game... follow this PlayStore link: " +
                        "http://play.google.com/store/apps/details?id=com.ideatemax.tictactoe";

                Intent sharingIntent = new Intent(android.content.Intent.ACTION_SEND);
                sharingIntent.setType("text/plain");

                sharingIntent.putExtra(android.content.Intent.EXTRA_SUBJECT, subject);
                sharingIntent.putExtra(android.content.Intent.EXTRA_TEXT, body);

                context.startActivity(Intent.createChooser(sharingIntent, "Share via..."));
            }
        });
    }


    /**
     * INTENT for starting SettingsActivity, callable when user presses on settings button.
     * @param imageBtn is the object to set listener on.
     */
    public static void setOnSettingsClickedListener(final MainActivity mainActivity,
                                                    ImageButton imageBtn){
        // Create new intent obj.
        final Intent intent = new Intent( (Context) mainActivity, SettingsActivity.class);

        final GameStats gameStats = GameStats.getInstance();

        // Intent fields
        final int SETTINGS_REQUEST = 123;

        imageBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // Put the necessary Extras.
                intent.putExtra(EXTRA_PLAYER_MODE, gameStats.getPlayerMode().toString())
                        .putExtra(EXTRA_DIFFICULTY, gameStats.getDifficulty().toString())
                        .putExtra(EXTRA_FIRST_MOVER, gameStats.getFirstMover().toString())
                        .putExtra(EXTRA_TOTAL_ROUNDS, gameStats.getNumberOfRounds())
                        .putExtra(EXTRA_X_NAME, gameStats.getPlayerX().getName())
                        .putExtra(EXTRA_O_NAME, gameStats.getPlayerO().getName());

                // Start Activity for results.
                mainActivity.startActivityForResult(intent, SETTINGS_REQUEST);
            }
        });
    }

}
