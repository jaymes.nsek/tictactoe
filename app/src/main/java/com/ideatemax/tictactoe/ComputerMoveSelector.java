/*
 * Copyright (c) 2020. Written by James Nsek
 */

package com.ideatemax.tictactoe;

import android.util.Log;

import java.util.HashMap;
import java.util.Map;
import java.util.Objects;
import java.util.Random;

import static com.ideatemax.tictactoe.GameStats.Difficulty.HIGH;
import static com.ideatemax.tictactoe.GameStats.Difficulty.MID;
import static com.ideatemax.tictactoe.Utils.BLOCK_FLAG;
import static com.ideatemax.tictactoe.Utils.WIN_FLAG;


/**
 * Given particular game params such as difficulty for example, the role of this class is to select
 * appropriate moves that is to be taken by computer from the Map populated by LogicManager
 * - Final class to prevent sub-class extension.
 */
final class ComputerMoveSelector {
    /**
     * Debugging tag used by the Android logger.
     */
    private final String TAG = getClass().getSimpleName();

    private static final String s_TAG = "ComputerMoveSelector";

    // Static singleton instance of ComputerMoveSelector
    private static ComputerMoveSelector move;

    private GameStats gameStats;


    // Private constructor to prevent the use of keyWord "new".
    private ComputerMoveSelector() {
        gameStats = GameStats.getInstance();
    }


    /**
     * Lazy initialisation, init "move" if null.
     */
    public static ComputerMoveSelector getInstance() {
        return (move == null) ? move = new ComputerMoveSelector() : move;
    }


    /**
     * Method is used for the selection of a move for the Computer. Includes at least 3 different
     * options from optionsHMap as allowed by difficulty and randomly choose one of these.
     *
     * There are two randomness to the pick:
     * 1. The first is in the selection of a value to insert into SelectionBox.
     * 2. The next is in the selection of a grid from shortlisted values in SelectionBox.
     * @return selected move which is played by computer.
     */
    public String pickFromSelectionBox(Map<String, Integer> optionsHMap,
                                               GameStats.Difficulty difficulty) {
        // HashMap with which to store possible moves in for later.
        Map<String, Integer> selectionBoxHMap = new HashMap<>();

        // Access playerO object reference.
        Player playerO = gameStats.getPlayerO();

        // declare return value for selectedMove.
        String selectedMove;

        // Retrieve currently held bestMove by playerO obj.
        int registeredBM = playerO.getBestMoveVal();

        // If bestMove is a WIN or FLAG, assign it as selectedMove.
        if (  (registeredBM == WIN_FLAG) || (registeredBM == -WIN_FLAG) ||
                (registeredBM == BLOCK_FLAG) || (registeredBM == -BLOCK_FLAG)  ) {
            // set return value for selectedMove.
            selectedMove = playerO.getBestMoveKey();
        }
        // else proceed to determine best move from minMaxOptionsHashMap.
        else {
            // Declare probMatrix array.
            int[] probMatrix = assignProbabilityMatrix(playerO, difficulty);

            // Init values in box to zero.
            int valuesInBox = 0;

            // Grid key placed here so that Array is not recreated on each go in the inner f-loop.
            Object[] gridKeys = optionsHMap.keySet().toArray();

            // Starting at index0 - retrieve each elemental probMatrix.
            for (int matrix : probMatrix) {
                // itr over the size of option contained in  optionsHMap.
                for (int j = 0; j < optionsHMap.size(); j++) {
                    // Randomly select gridKeys for testing against probMatrix, this ensures that
                    // when the same choices occur their addition to selectionBox are randomised.
                    assert gridKeys != null;
                    String key = (String) gridKeys[new Random().nextInt(gridKeys.length)];

                    // Adds the itr key to SelectionBox if it matches matrix and inc valuesInBox.
                    if (optionsHMap.get(key) == matrix) {
                        // insert grid in selectionBox.
                        selectionBoxHMap.put(key, Objects.requireNonNull(optionsHMap.get(key)));

                        // increment the total values inserted by 1.
                        valuesInBox++;

                        // Break away from inner-loop if values in box reaches 3 before the
                        // values in optionsHMap are exhausted.
                        if (valuesInBox == 3)
                            break;
                    }
                }
                // Enforce break from outer-loop if three values already picked.
                if (valuesInBox == 3)
                    break;
            }
            // Randomly select grid to play and send result back to calling method.
            Object[] boxKeys = selectionBoxHMap.keySet().toArray();

            // return a random key from boxKeys.
            assert boxKeys != null;
            selectedMove = (String) boxKeys[new Random().nextInt(boxKeys.length)];
        }

        return selectedMove;
    }


    /**
     * Assign appropriate probMatrix depending on Difficulty & Algorithm combination.
     */
    private int[] assignProbabilityMatrix(Player playerO, GameStats.Difficulty difficulty) {
        int[] probMatrix;

        if (playerO.getAlgorithm().equals(Player.Algorithm.MIN)) {
            if (difficulty.equals(HIGH)) {
                probMatrix = new int[]{-3, -2, -1, 0, 1, 2, 3, 4};
            }
            else if (difficulty.equals(MID)) {
                probMatrix = new int[]{0, 1, -1, 2, -2, 3, 4, -3};
            }
            else {
                probMatrix = new int[]{2, 1, 0, 3, -1, 4, -2, -3};
            }
        }
        // Else MAX_ALGORITHM is applicable.
        else {
            if (difficulty.equals(HIGH)) {
                probMatrix = new int[]{4, 3, 2, 1, 0, -1, -2, -3};
            }
            else if (difficulty.equals(MID)) {
                probMatrix = new int[]{2, 1, 0, 3, 4, -1, -2, -3};
            }
            else {
                probMatrix = new int[]{0, 1, -1, -2, 2, 3, 4, -3};
            }
        }

        return probMatrix;
    }

}
