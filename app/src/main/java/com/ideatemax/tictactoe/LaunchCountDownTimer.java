/*
 * Copyright (c) 2020. Written by James Nsek
 */

package com.ideatemax.tictactoe;

import android.os.CountDownTimer;
import android.util.Log;

/**
 * Custom CountDownTimer to delay refresh of screen so that currentPlayer info has
 *  time to print and is acknowledged by user.
 */
public class LaunchCountDownTimer extends CountDownTimer {
    /**
     * Debugging tag used by the Android logger.
     */
    private final String TAG = getClass().getSimpleName();

    // Instance of MainActivity context.
    private MyActivityLifecycleCallbacks callbacks;

    private LogicManager.ComputerMoves computerMoves;

    /**
     * @param millisInFuture    The number of millis in the future from the call to {@link #start()}
     *                          until the countdown is done and {@link #onFinish()} is called.
     * @param countDownInterval The interval along the way to receive
     *                          {@link #onTick(long)} callbacks.
     */
    public LaunchCountDownTimer(long millisInFuture, long countDownInterval) {
        super(millisInFuture, countDownInterval);

        // Instantiate computerMoves obj so that methods may be called on it.
        computerMoves = LogicManager.getInstance().new ComputerMoves();
    }

    @Override
    public void onTick(long millisUntilFinished) {
        // DO NOTHING
    }

    @Override
    public void onFinish() {
        //make call to playComputerMove() and use the return value of
        // onComputerTurn as argument so that the computer can make first move if applicable.
        computerMoves.playComputerMove();
    }
}
