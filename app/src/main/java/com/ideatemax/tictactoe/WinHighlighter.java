/*
 * Copyright (c) 2020. Written by James Nsek
 */

package com.ideatemax.tictactoe;

/**
 * Enum to use with colour_or_decolour_winCombo() method which highlights win combination,
 * and ensures that passable params are restricted..
 */
public enum WinHighlighter {
    COLOUR,
    DECOLOUR,
}
