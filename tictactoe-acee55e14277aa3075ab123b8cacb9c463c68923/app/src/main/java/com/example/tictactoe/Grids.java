package com.example.tictactoe;


import androidx.annotation.NonNull;

/**
 * This is the class Grid - used for holding three string variables representing win combination
 */

public class Grids {

    //INSTANCE VARIABLES
    private String first;
    private String second;
    private String third;

    //PUBLIC CONSTRUCTORS
    public Grids (String first, String second, String third){
        this.first = first;
        this.second = second;
        this.third = third;
    }

    @NonNull
    @Override
    public String toString() {
        return this.first + " " + this.second + " " + this.third;
    }

    //ACCESSORS -  getMethods for retrieving the grid coordinates

    public String getFirst() {
        return first;
    }

    public String getSecond() {
        return second;
    }

    public String getThird() {
        return third;
    }

}
