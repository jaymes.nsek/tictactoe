/*
 * Copyright (c) 2020. Written by James Nsek
 */

package com.ideatemax.tictactoe;


import androidx.annotation.NonNull;

/**
 * This class is - used for holding three string variables representing win combination
 */

public class WinCase {

    // INSTANCE VARIABLES

    private String first;
    private String second;
    private String third;


    // PUBLIC CONSTRUCTORS
    public WinCase(String first, String second, String third){
        this.first = first;
        this.second = second;
        this.third = third;
    }

    @NonNull
    @Override
    public String toString() {
        return this.first + " " + this.second + " " + this.third;
    }

    //ACCESSORS -  getMethods for retrieving the grid coordinates

    public String getFirst() {
        return first;
    }

    public String getSecond() {
        return second;
    }

    public String getThird() {
        return third;
    }

}
