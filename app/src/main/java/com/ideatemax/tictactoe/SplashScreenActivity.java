/*
 * Copyright (c) 2020. Written by James Nsek
 */

package com.ideatemax.tictactoe;


import android.content.Intent;
import android.os.Bundle;
import android.os.CountDownTimer;

import androidx.appcompat.app.AppCompatActivity;

public class SplashScreenActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash_screen);
    }

    @Override
    protected void onPostResume() {
        super.onPostResume();
        // Create an anonymous CountDownTimer for delaying refresh of screen.
        CountDownTimer delayRefresh = new CountDownTimer(1600, 1600) {

            public void onTick(long millisUntilFinished) {
                // DO NOTHING
            }

            public void onFinish() {
                startIntent();
            }
        };
        delayRefresh.start();
    }


    /**
     * Intent for starting MainActivity.
     */
    private void startIntent(){
        Intent intent = new Intent(getApplicationContext(),
                MainActivity.class);
        startActivity(intent);
        finish();
    }

}
