/*
 * Copyright (c) 2020. Written by James Nsek
 */

package com.ideatemax.tictactoe;

import android.app.Activity;
import android.os.CountDownTimer;
import android.util.Log;
import android.widget.Button;
import androidx.fragment.app.DialogFragment;
import androidx.fragment.app.FragmentManager;
import static com.ideatemax.tictactoe.GameStats.PlayerMode.ONE;
import static com.ideatemax.tictactoe.MainActivity.GAME_DRAW;
import static com.ideatemax.tictactoe.MainActivity.mCod0;
import static com.ideatemax.tictactoe.MainActivity.mCod1;
import static com.ideatemax.tictactoe.MainActivity.mCod2;
import static com.ideatemax.tictactoe.OtherViewsContainer.TV_KEY_CURRENT_PLAYER;
import static com.ideatemax.tictactoe.OtherViewsContainer.TV_KEY_MODE_DIFF;
import static com.ideatemax.tictactoe.OtherViewsContainer.TV_KEY_O_NAME;
import static com.ideatemax.tictactoe.OtherViewsContainer.TV_KEY_O_TALLY;
import static com.ideatemax.tictactoe.OtherViewsContainer.TV_KEY_ROUND_VAL;
import static com.ideatemax.tictactoe.OtherViewsContainer.TV_KEY_X_NAME;
import static com.ideatemax.tictactoe.OtherViewsContainer.TV_KEY_X_TALLY;
import static com.ideatemax.tictactoe.Player.PHONE_O_NAME;
import static com.ideatemax.tictactoe.Player.PLAYER_O_NAME;
import static com.ideatemax.tictactoe.Player.PLAYER_X_NAME;
import static com.ideatemax.tictactoe.Player.X_SYM;

final class UiUpdateManager {
    /**
     * Debugging tag used by the Android logger.
     */
    private final String TAG = getClass().getSimpleName();

    private GameStats gameStats;

    private static String winnerDIALOG_FLAG;

    private static String winnerNameDIALOG_FLAG;

    private static int totalNumOfRoundsDIALOG_FLAG;

    private static int xTallyDIALOG_FLAG;

    private static int oTallyDIALOG_FLAG;

    private MyActivityLifecycleCallbacks mCallbacks;

    // Single allowable instance of UiUpdateManager.
    private static UiUpdateManager updateManager;


    /**
     * Private constructor to prevent instantiation with "new" keyword.
     */
    private UiUpdateManager() {
        // Instantiate a gameStats object upon which getters may be called later on for data.
        gameStats = GameStats.getInstance();
    }


    /**
     * Reactive initialiser.
     */
    public static UiUpdateManager getInstance(){
        return (updateManager == null) ? updateManager = new UiUpdateManager() : updateManager;
    }


    public void registerForCallbacksStates(MyActivityLifecycleCallbacks callbacks) {
        mCallbacks = callbacks;
    }


    /**
     * Method prints current player name and symbol (identifier) to MainActivity.
     */
    public void displayCurrentPlayer() {
        try{
            OtherViewsContainer container = OtherViewsContainer.getInstance();

            // Update the mDisplayCurrentPlayerValueTextView to X or O.
            container.getTextView(TV_KEY_CURRENT_PLAYER)
                    .setText( gameStats.getCurrentPlayer().equals(X_SYM) ?
                            gameStats.getPlayerX().getName() : gameStats.getPlayerO().getName());
        } catch (Exception e) {
            Log.e(TAG, "displayCurrentPlayer: ", e);
        }
    }


    /**
     * Update round scores to screen when a win case is matched.
     * @param player is the Player obj's tally to update.
     */
    public void updateTallyInfo(Player player) {
        String tally, name;

        OtherViewsContainer container = OtherViewsContainer.getInstance();

        // Display and update playerX info.
        if (player.getSym().equals(X_SYM)) {
            // Display playerX name and tally.
            container.getTextView(TV_KEY_X_NAME).setText(player.getName());

            container.getTextView(TV_KEY_X_TALLY).setText(Integer.toString(player.getTally()));
        }
        // Display and update playerO info.
        else {
            // Display playerO name and tally.
            container.getTextView(TV_KEY_O_NAME).setText(player.getName());

            container.getTextView(TV_KEY_O_TALLY).setText(Integer.toString(player.getTally()));
        }
    }


    /**
     * Method updates mode and difficulty info to MainActivity.
     */
    public void updateModeAndDiff() {
        // modeAndDiffParams is concatenated in the if-else statements and the Mode and Difficulty
        // TextView updated.
        String modeAndDiffParams = gameStats.getPlayerMode().toUiString();

        OtherViewsContainer container = OtherViewsContainer.getInstance();

        // When in one player mode.
        if (gameStats.getPlayerMode() == ONE) {
            // Concatenate values in a ONE mode.
            modeAndDiffParams += " | " + gameStats.getDifficulty().toString();
        }

        // Convert string to caps and update display with text concatenation.
        modeAndDiffParams = modeAndDiffParams.toUpperCase();

        container.getTextView(TV_KEY_MODE_DIFF).setText(modeAndDiffParams);
    }


    /**
     * Method is used for resetting grid colours back to their default values between rounds after
     * a win is matched and the colour changed to clearly indicate winCombo.
     * @param action colour or decolour matched winCombo.
     */
    public void colour_or_decolour_winCombo(WinHighlighter action) {
        Activity activity = mCallbacks.getCurrentActivity();

        GridsContainer.Grid[] gridsArr = new GridsContainer.Grid[3];

        // dot uppercase is required to match ENUM const.
        gridsArr[0] = GridsContainer.Grid.valueOf(mCod0.toUpperCase());
        gridsArr[1] = GridsContainer.Grid.valueOf(mCod1.toUpperCase());
        gridsArr[2] = GridsContainer.Grid.valueOf(mCod2.toUpperCase());

        // Create a local button object
        Button myButt;

        GridsContainer container = GridsContainer.getInstance();

        // Depending on action, change colours reset the TextView's textColours.
        if (action.equals(WinHighlighter.COLOUR)) {
            for (GridsContainer.Grid grid : gridsArr) {
                myButt = (Button) container.getButtonFromId(grid);
                myButt.setTextColor(activity.getResources().getColor(R.color.grid_win_text_colour));
            }
        } else if (action.equals(WinHighlighter.DECOLOUR)) {
            for (GridsContainer.Grid grid : gridsArr) {
                myButt = (Button) container.getButtonFromId(grid);
                myButt.setTextColor(activity.getResources().getColor(R.color.grid_text_colour));
            }
        }
    }


    /**
     * Method updates current round info to MainActivity.
     */
    public void updateRound() {
        OtherViewsContainer container = OtherViewsContainer.getInstance();

        // Concatenate.
        String text = gameStats.getCurrentRound() + " of " + gameStats.getNumberOfRounds();
        // Print to screen.
        container.getTextView(TV_KEY_ROUND_VAL).setText(text);
    }


    /**
     * Handles win/end-of-game dialog.
     */
    public void showNoticeDialog(final FragmentManager fManager, String winner, String winnerName,
                                 int totalNumOfRounds, int xTally, int oTally) {
        // Retrieve the MainActivity.
        final Activity activity = mCallbacks.getCurrentActivity();

        // assign received values to the global var so that we may use it with mDelayRefresh
        winnerDIALOG_FLAG = winner;
        winnerNameDIALOG_FLAG = winnerName;
        totalNumOfRoundsDIALOG_FLAG = totalNumOfRounds;
        xTallyDIALOG_FLAG = xTally;
        oTallyDIALOG_FLAG = oTally;

        // Get current sound state.
        final boolean isSoundSwitchON = gameStats.getIsSoundSwitchON();

        // Create an anonymous CountDownTimer for delaying refresh of screen, invoke MediaPlayer,
        // and create an instance of the dialog fragment and show it.
        CountDownTimer mDelayRefresh = new CountDownTimer(3000, 2000)
        {
            public void onTick(long millisUntilFinished) {
                // invoke drawDialog MediaPlayer if game is a draw and soundSwitch is set to TRUE.
                if (winnerDIALOG_FLAG.equals(GAME_DRAW) && (isSoundSwitchON)) {
                    SoundManager.getInstance(activity, activity
                            .findViewById(R.id.soundToggleIBtn)).invokeOnDrawnSound(activity);
                }
                // else winner should be either PlayerX or O, invoke winDialog MediaPlayer.
                else if (isSoundSwitchON){
                    SoundManager.getInstance(activity, activity
                            .findViewById(R.id.soundToggleIBtn)).invokeOnGameWinSound(activity);
                }
            }

            public void onFinish() {
                // Create an instance of the dialog fragment and show it
                // fManager =  getSupportFragmentManager()
                DialogFragment dialog =
                        new GameWonDialogFragment(winnerDIALOG_FLAG, winnerNameDIALOG_FLAG,
                                totalNumOfRoundsDIALOG_FLAG, xTallyDIALOG_FLAG, oTallyDIALOG_FLAG);
                dialog.show(fManager, "GameWonDialogFragment");
            }
        };
        mDelayRefresh.start();
    }

}
